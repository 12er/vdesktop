﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace VDesktop
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Environment.CurrentDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            Settings.TouchSettingsDirectory();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            DesktopsForm f = DesktopsForm.Get();
            f.Show();

            WindowRecovery.Get().RestoreWindows();
            WindowRecovery.Get().StartSavePeriodically();

            VDesktopForm form = VDesktopForm.Get();
            Application.Run(form);
        }
    }

}
