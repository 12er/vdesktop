﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Reflection;
using VDesktop.NativeCalls;

namespace VDesktop
{
    public partial class VDesktopForm : Form, DTSelectCallback, AddCallback, RemoveCallback, ShowSettingsCallback, ExitCallback
    {

        private static VDesktopForm form = null;

        public static VDesktopForm Get()
        {
            if (form == null)
            {
                form = new VDesktopForm();
            }
            return form;
        }

        private OverviewTray overviewTray;
        private List<DesktopTray> Dtrays;

        public VDesktopForm()
        {
            InitializeComponent();
            WindowState = FormWindowState.Minimized;
            ShowInTaskbar = false;
            Dtrays = new List<DesktopTray>();
            FormClosed += new FormClosedEventHandler(VDesktopClosing);
        }

        private void VDesktopForm_Load(object sender, EventArgs e)
        {
            overviewTray = new OverviewTray();
            overviewTray.SetAddCallback(this);
            overviewTray.SetRemoveCallback(this);
            overviewTray.SetShowSettingsCallback(this);
            overviewTray.SetExitCallback(this);
            overviewTray.Update();

            VDesktopManager vdm = VDesktopManager.Get();
            List<VDesktop> desktops = vdm.GetDesktops();
            foreach (VDesktop desk in desktops)
            {
                uint ID = desk.getID();
                DesktopTray tray = new DesktopTray(ID);
                tray.SetAddCallback(this);
                tray.SetRemoveCallback(this);
                tray.SetExitCallback(this);
                tray.SetShowSettingsCallback(this);
                tray.SetDTSelectCallback(this);
                if (Dtrays.Count() == 0)
                {
                    tray.Select();
                }
                Dtrays.Add(tray);
                tray.Update();
            }
        }

        public List<DesktopTray> GetDesktopTrays()
        {
            return Dtrays;
        }

        public void AddDesktop()
        {
            //add desktop
            VDesktopManager vdm = VDesktopManager.Get();
            List<VDesktop> desktops = vdm.GetDesktops();
            uint newID = (uint)desktops.Count() + 1;
            vdm.AddDesktop(newID);

            //add tray icon of desktop
            DesktopTray tray = new DesktopTray(newID);
            tray.SetAddCallback(this);
            tray.SetRemoveCallback(this);
            tray.SetShowSettingsCallback(this);
            tray.SetExitCallback(this);
            tray.SetDTSelectCallback(this);
            Dtrays.Add(tray);
            tray.Update();
        }

        public void RemoveDesktop()
        {
            //remove desktop
            VDesktopManager vdm = VDesktopManager.Get();
            List<VDesktop> desktops = vdm.GetDesktops();
            if (desktops.Count() <= 1)
            {
                //no desktops for removal available
                //stop here, and warn user
                MessageBox.Show("The last virtual desktop can't be removed.", "VDesktop - Add new virtual desktops", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                return;
            }
            uint delID = (uint)desktops.Count();
            vdm.RemoveDesktop(delID);
            VDesktop currDesktop = vdm.GetCurrentDesktop();

            //remove tray icon of desktop
            DesktopTray tray = Dtrays.Last();
            Dtrays.Remove(tray);
            tray.Dispose();
            DesktopTray selectedTray = Dtrays.Find(x => x.GetID() == currDesktop.getID());
            selectedTray.Select();
        }

        public void RemoveDesktop(uint ID)
        {
            DesktopsForm form = DesktopsForm.Get();
            form.Invoke(new Action(() =>
                {
                    form.RemoveDesktop((int)ID);
                    form.Invalidate();
                }));
        }

        public void ShowSettings()
        {
            SettingsForm settingsForm = SettingsForm.Get();
            settingsForm.Show();
        }

        public void Exit()
        {
            WindowRecovery.Get().ForcedSaveWindows();
            HotKeyManager.Get().UnregisterHotKeys();
            Close();
        }

        public void Selected()
        {
            foreach (DesktopTray tray in Dtrays)
            {
                tray.Deselect();
            }
        }

        private void VDesktopClosing(object o, FormClosedEventArgs args)
        {
            VDesktopManager vdm = VDesktopManager.Get();
            vdm.Dispose();
            overviewTray.Dispose();
            foreach (DesktopTray tray in Dtrays)
            {
                tray.Dispose();
            }
        }

    }

    public interface AddCallback
    {
        void AddDesktop();
    }

    public interface RemoveCallback
    {
        void RemoveDesktop();
        void RemoveDesktop(uint ID);
    }

    public interface ShowSettingsCallback
    {
        void ShowSettings();
    }

    public interface ExitCallback
    {
        void Exit();
    }

    public interface DTSelectCallback
    {
        void Selected();
    }

    public class OverviewTray
    {
        private static int mouseX;
        private static int mouseY;
        private static long lastMouseUpdate;

        private AddCallback addCallback = null;
        private RemoveCallback removeCallback = null;
        private ShowSettingsCallback showSettingsCallback = null;
        private ExitCallback exitCallback = null;

        private NotifyIcon tray;

        public OverviewTray()
        {
            tray = new NotifyIcon();
            tray.MouseMove += new MouseEventHandler(MoveEvent);

            ContextMenu menu = new ContextMenu();
            MenuItem addItem = new MenuItem();
            addItem.Text = "Add Desktop";
            addItem.Click += new EventHandler(AddEvent);
            menu.MenuItems.Add(addItem);
            MenuItem removeItem = new MenuItem();
            removeItem.Text = "Remove Desktop";
            removeItem.Click += new EventHandler(RemoveEvent);
            menu.MenuItems.Add(removeItem);
            menu.MenuItems.Add("-");
            MenuItem showSettingsItem = new MenuItem();
            showSettingsItem.Text = "Settings";
            showSettingsItem.Click += new EventHandler(ShowSettingsEvent);
            menu.MenuItems.Add(showSettingsItem);
            menu.MenuItems.Add("-");
            MenuItem exitItem = new MenuItem();
            exitItem.Text = "Exit";
            exitItem.Click += new EventHandler(ExitEvent);
            menu.MenuItems.Add(exitItem);

            tray.ContextMenu = menu;
        }

        public void SetAddCallback(AddCallback callback)
        {
            this.addCallback = callback;
        }

        private void AddEvent(object o, EventArgs args)
        {
            if (addCallback != null)
            {
                addCallback.AddDesktop();
                DesktopsForm overview = DesktopsForm.Get();
                overview.Invoke(new Action(() => 
                { 
                    if (overview.Visible && overview.WindowState != FormWindowState.Minimized) 
                    {
                        overview.Left -= overview.GetPreviewWidth();
                        overview.Present(); 
                        overview.Invalidate(); 
                    } 
                }));
            }
        }

        public void SetRemoveCallback(RemoveCallback callback)
        {
            this.removeCallback = callback;
        }

        private void RemoveEvent(object o, EventArgs args)
        {
            if (removeCallback != null)
            {
                removeCallback.RemoveDesktop();
                DesktopsForm overview = DesktopsForm.Get();
                overview.Invoke(new Action(() => 
                { 
                    if (overview.Visible && overview.WindowState != FormWindowState.Minimized) 
                    {
                        overview.Left += overview.GetPreviewWidth();
                        overview.Present(); 
                        overview.Invalidate(); 
                    } 
                }));
            }
        }

        public void SetShowSettingsCallback(ShowSettingsCallback callback)
        {
            this.showSettingsCallback = callback;
        }

        private void ShowSettingsEvent(object o, EventArgs args)
        {
            if (showSettingsCallback != null)
            {
                showSettingsCallback.ShowSettings();
            }
        }

        public void SetExitCallback(ExitCallback callback)
        {
            this.exitCallback = callback;
        }

        private void ExitEvent(object o, EventArgs args)
        {
            if (exitCallback != null)
            {
                exitCallback.Exit();
            }
        }

        private void MoveEvent(object o, MouseEventArgs args)
        {
            Point p = System.Windows.Forms.Cursor.Position;
            mouseX = p.X;
            mouseY = p.Y;
            lastMouseUpdate = System.DateTime.Now.ToUniversalTime().Ticks;
            System.Threading.Timer timer1 = new System.Threading.Timer(ShowDesktopsEvent, null, 10, 0);
            System.Threading.Timer timer2 = new System.Threading.Timer(ShowDesktopsEvent, null, 500, 0);
        }

        private void ShowDesktopsEvent(object o)
        {
            //check if the context menu of the NotifyIcon tray is opened
            //if it is, don't show/hide desktops overview
            FieldInfo notifyIconNativeWindowInfo = typeof(NotifyIcon).GetField("window", BindingFlags.NonPublic | BindingFlags.Instance);
            NativeWindow trayWindow = (NativeWindow)notifyIconNativeWindowInfo.GetValue(tray);
            if (trayWindow != null && trayWindow.Handle != IntPtr.Zero && User32.IsWindow(trayWindow.Handle))
            {
                //if the menu is visible, the notifyicon is the foreground window
                bool menuIsVisible = User32.GetForegroundWindow() == trayWindow.Handle;
                if (menuIsVisible)
                {
                    //menu is visible, hence don't change desktop overview state
                    return;
                }
            }

            Point p = System.Windows.Forms.Cursor.Position;
            long time = (System.DateTime.Now.ToUniversalTime().Ticks - lastMouseUpdate) / 10000;
            if (time > 5)
            {
                DesktopsForm overview = DesktopsForm.Get();
                if (Math.Sqrt(Math.Pow(p.X - mouseX, 2) + Math.Pow(p.Y - mouseY, 2)) < 16.0)
                {
                    overview.Invoke(new Action(() => { overview.Present(new Point(mouseX, mouseY)); }));
                }
                else if (!overview.isShowingByKeyboard())
                {
                    overview.Invoke(new Action(() => { overview.RequestStopPresenting(); }));
                }
            }
        }

        public void Dispose()
        {
            tray.Dispose();
        }

        public void Update()
        {
            tray.Icon = CreateIcon();
            tray.Text = "VDesktop";
            tray.Visible = true;
        }

        private Icon CreateIcon()
        {
            Bitmap bitmap = new Bitmap("./VDesktop.png");
            IntPtr iconHandle = bitmap.GetHicon();
            Icon icon = Icon.FromHandle(iconHandle);

            return icon;
        }

    }

    public class DesktopTray
    {

        private static int mouseX;
        private static int mouseY;
        private static long lastMouseUpdate;

        private uint ID;
        private NotifyIcon tray;
        private bool selected;

        private AddCallback addCallback = null;
        private RemoveCallback removeCallback = null;
        private ShowSettingsCallback showSettingsCallback = null;
        private ExitCallback exitCallback = null;
        private DTSelectCallback dtSelectCallback = null;

        public DesktopTray(uint ID)
        {
            this.ID = ID;
            this.selected = false;
            tray = new NotifyIcon();
            tray.Click += new EventHandler(ClickEvent);
            tray.MouseMove += new MouseEventHandler(MoveEvent);

            ContextMenu menu = new ContextMenu();
            MenuItem addItem = new MenuItem();
            addItem.Text = "Add Desktop";
            addItem.Click += new EventHandler(AddEvent);
            menu.MenuItems.Add(addItem);
            MenuItem removeItem = new MenuItem();
            removeItem.Text = "Remove Desktop";
            removeItem.Click += new EventHandler(RemoveEvent);
            menu.MenuItems.Add(removeItem);
            menu.MenuItems.Add("-");
            MenuItem showSettingsItem = new MenuItem();
            showSettingsItem.Text = "Settings";
            showSettingsItem.Click += new EventHandler(ShowSettingsEvent);
            menu.MenuItems.Add(showSettingsItem);
            menu.MenuItems.Add("-");
            MenuItem exitItem = new MenuItem();
            exitItem.Text = "Exit";
            exitItem.Click += new EventHandler(ExitEvent);
            menu.MenuItems.Add(exitItem);
            
            tray.ContextMenu = menu;
        }

        public uint GetID()
        {
            return ID;
        }

        public void SetAddCallback(AddCallback callback)
        {
            this.addCallback = callback;
        }

        private void AddEvent(object o, EventArgs args)
        {
            if (addCallback != null)
            {
                addCallback.AddDesktop();
                DesktopsForm overview = DesktopsForm.Get();
                overview.Invoke(new Action(() => 
                { 
                    if (overview.Visible && overview.WindowState != FormWindowState.Minimized) 
                    {
                        overview.Left -= overview.GetPreviewWidth();
                        overview.Present(); 
                        overview.Invalidate(); 
                    } 
                }));
            }
        }

        public void SetRemoveCallback(RemoveCallback callback)
        {
            this.removeCallback = callback;
        }

        private void RemoveEvent(object o, EventArgs args)
        {
            if (removeCallback != null)
            {
                removeCallback.RemoveDesktop(ID);
            }
        }

        public void SetShowSettingsCallback(ShowSettingsCallback callback)
        {
            this.showSettingsCallback = callback;
        }

        private void ShowSettingsEvent(object o, EventArgs args)
        {
            if (showSettingsCallback != null)
            {
                showSettingsCallback.ShowSettings();
            }
        }

        public void SetExitCallback(ExitCallback callback)
        {
            this.exitCallback = callback;
        }

        private void ExitEvent(object o, EventArgs args)
        {
            if (exitCallback != null)
            {
                exitCallback.Exit();
            }
        }

        public void SetDTSelectCallback(DTSelectCallback callback)
        {
            this.dtSelectCallback = callback;
        }

        private void MoveEvent(object o, MouseEventArgs args)
        {
            Point p = System.Windows.Forms.Cursor.Position;
            mouseX = p.X;
            mouseY = p.Y;
            lastMouseUpdate = System.DateTime.Now.ToUniversalTime().Ticks;
            System.Threading.Timer timer = new System.Threading.Timer(ShowDesktopsEvent, null, 500, 0);
        }

        private void ShowDesktopsEvent(object o)
        {
            //check if the context menu of the NotifyIcon tray is opened
            //if it is, don't show/hide desktops overview
            FieldInfo notifyIconNativeWindowInfo = typeof(NotifyIcon).GetField("window", BindingFlags.NonPublic | BindingFlags.Instance);
            NativeWindow trayWindow = (NativeWindow)notifyIconNativeWindowInfo.GetValue(tray);
            if (trayWindow != null && trayWindow.Handle != IntPtr.Zero && User32.IsWindow(trayWindow.Handle))
            {
                //if the menu is visible, the notifyicon is the foreground window
                bool menuIsVisible = User32.GetForegroundWindow() == trayWindow.Handle;
                if (menuIsVisible)
                {
                    //menu is visible, hence don't change desktop overview state
                    return;
                }
            }

            Point p = System.Windows.Forms.Cursor.Position;
            long time = (System.DateTime.Now.ToUniversalTime().Ticks - lastMouseUpdate) / 10000;
            if (time > 495)
            {
                DesktopsForm overview = DesktopsForm.Get();
                if (Math.Sqrt(Math.Pow(p.X - mouseX, 2) + Math.Pow(p.Y - mouseY, 2)) < 16.0)
                {
                    overview.Invoke(new Action(() => { overview.Present(new Point(mouseX, mouseY)); }));
                }
                else if(!overview.isShowingByKeyboard())
                {
                    overview.Invoke(new Action(() => { overview.RequestStopPresenting(); }));
                }
            }
        }

        private void ClickEvent(object o, EventArgs args)
        {
            MouseButtons button = MouseButtons.Left;
            try
            {
                MouseEventArgs e = (MouseEventArgs)args;
                button = e.Button;
            }
            catch (Exception)
            {
            }
            if (button == MouseButtons.Left)
            {
                DesktopsForm form = DesktopsForm.Get();
                form.SelectDesktop((int)ID);
                Select();
            }
        }

        public void Select()
        {
            if (dtSelectCallback != null)
            {
                dtSelectCallback.Selected();
            }
            selected = true;
            Update();
            VDesktopManager vdm = VDesktopManager.Get();
            vdm.Show(ID);
        }

        public void Deselect()
        {
            selected = false;
            Update();
        }

        public void Dispose()
        {
            tray.Dispose();
        }

        public void Update()
        {
            tray.Icon = CreateIcon();
            tray.Text = "Desktop " + ID;
            tray.Visible = true;
        }

        private Icon CreateIcon()
        {
            //create renderer
            Bitmap iconBitmap = new Bitmap(16, 16);
            Graphics iconRenderer = Graphics.FromImage(iconBitmap);
            iconRenderer.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;

            //render something into the bitmap
            Font iconFont = null;
            if (selected)
            {
                iconFont = new Font("Tahoma", 10, FontStyle.Bold);
            }
            else
            {
                iconFont = new Font("Tahoma", 8);
            }
            SolidBrush iconBrush = new SolidBrush(Color.White);
            iconRenderer.DrawString("" + ID, iconFont, iconBrush, 0, 0);

            //finally create icon from bitmap
            IntPtr iconHandle = iconBitmap.GetHicon();
            Icon icon = Icon.FromHandle(iconHandle);

            //clean up resources
            iconBrush.Dispose();
            iconFont.Dispose();
            iconBitmap.Dispose();
            iconRenderer.Dispose();

            return icon;
        }

    }

    public class DesktopsForm : Form
    {

        private static DesktopsForm form = null;

        public static DesktopsForm Get()
        {
            if (form == null)
            {
                form = new DesktopsForm();
                form.WindowState = FormWindowState.Minimized;
            }
            return form;
        }

        private DesktopEmphasizer desktopEmphasizer = null;
        private SmoothEmphasizer smoothEmphasizer = null;
        private NoEmphasizer noEmphasizer = null;
        private BorderedEmphasizer borderedEmphasizer = null;

        private bool useAero = true;
        private bool useAddCloseButtons = true;
        private bool useBorders = true;
        private bool useTooltip = true;

        private WindowPainter windowPainter = null;
        private BasicPainter basicPainter = null;
        private AeroPainter aeroPainter = null;
        private CachedAeroPainter cachedAeroPainter = null;

        private int previewWidth = 235;
        private int previewHeight = 150;
        private int previewMargin = 10;
        private bool settingPreviewDimensions = false;

        private long lastMouseUpdate;
        private Point lastPresentPoint = new Point(1000000,1000000);
        private int mouseX;
        private int mouseY;
        private int leftMouseX;
        private int leftMouseY;
        private bool leftMouseIsDown = false;
        private int rightMouseX;
        private int rightMouseY;
        private bool rightMouseIsDown = false;
        private bool isDragging = false;
        private bool showingByKeyboard = false;

        private bool closedGreatestDesktop = false;
        private Icon closeButtonIcon_unfocused = null;
        private Icon closeButtonIcon_focused = null;
        private Icon closeButtonIcon_clicked = null;
        private Icon addButtonIcon_unfocused = null;
        private Icon addButtonIcon_focused = null;
        private Icon addButtonIcon_clicked = null;
        private int hoveredDesktop = -1;
        private bool hoveringClose = false;
        private bool clickingClose = false;
        private bool hoveringAdd = false;
        private bool clickingAdd = false;

        private ToolTip toolTip = null;
        private bool showTooltip = true;

        private VDesktopWindow dragSelection = null;
        private Point dragMousePos;
        private Point relativeDragMousePos;
        private int dragFromDesktop;

        public DesktopsForm()
            : base()
        {
            Update();
        }

        protected override void OnLoad(EventArgs e)
        {
            ShowInTaskbar = false;
            FormBorderStyle = FormBorderStyle.FixedSingle;
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            useAero = Settings.Get().GetUseAero();
            UpdateAero();

            //init contextmenu
            ContextMenu menu = new ContextMenu();
            MenuItem addItem = new MenuItem();
            addItem.Text = "Add Desktop";
            addItem.Click += new EventHandler(AddEvent);
            menu.MenuItems.Add(addItem);
            MenuItem removeItem = new MenuItem();
            removeItem.Text = "Remove Desktop";
            removeItem.Click += new EventHandler(RemoveEvent);
            menu.MenuItems.Add(removeItem);
            menu.MenuItems.Add("-");
            MenuItem showSettingsItem = new MenuItem();
            showSettingsItem.Text = "Settings";
            showSettingsItem.Click += new EventHandler(ShowSettingsEvent);
            menu.MenuItems.Add(showSettingsItem);
            ContextMenu = menu;

            //calculate preview width and height
            SetPreviewHeight(Settings.Get().GetPreviewHeight());
            Resize += ResizePreview;

            //init desktop emphasizers
            smoothEmphasizer = new SmoothEmphasizer();
            borderedEmphasizer = new BorderedEmphasizer();
            noEmphasizer = new NoEmphasizer();
            switch (Settings.Get().GetEmphasizeDesktop())
            {
                case EmphasizeDesktop.BorderedEmphasizer:
                    desktopEmphasizer = borderedEmphasizer;
                    break;
                case EmphasizeDesktop.SmoothEmphasizer:
                    desktopEmphasizer = smoothEmphasizer;
                    break;
                case EmphasizeDesktop.NoEmphasizer:
                    desktopEmphasizer = noEmphasizer;
                    break;
                default:
                    desktopEmphasizer = borderedEmphasizer;
                    break;
            }

            //init window painters
            basicPainter = new BasicPainter();
            aeroPainter = new AeroPainter(this.Handle); 
            cachedAeroPainter = new CachedAeroPainter(this.Handle, Settings.Get().GetUpdateThumbnailRate());
            switch (Settings.Get().GetThumbnailView())
            {
                case ThumbnailView.CachedAeroWindows:
                    windowPainter = cachedAeroPainter;
                    break;
                case ThumbnailView.AeroWindows:
                    windowPainter = aeroPainter;
                    break;
                case ThumbnailView.BasicWindows:
                    windowPainter = basicPainter;
                    break;
                default:
                    windowPainter = cachedAeroPainter;
                    break;
            }

            //init button icons
            closeButtonIcon_focused = Icon.FromHandle(new Bitmap("./closebutton_focused.png").GetHicon());
            closeButtonIcon_unfocused = Icon.FromHandle(new Bitmap("./closebutton_unfocused.png").GetHicon());
            closeButtonIcon_clicked = Icon.FromHandle(new Bitmap("./closebutton_clicked.png").GetHicon());
            addButtonIcon_focused = Icon.FromHandle(new Bitmap("./addbutton_focused.png").GetHicon());
            addButtonIcon_unfocused = Icon.FromHandle(new Bitmap("./addbutton_unfocused.png").GetHicon());
            addButtonIcon_clicked = Icon.FromHandle(new Bitmap("./addbutton_clicked.png").GetHicon());

            //make some windows visible on all desktops
            IntPtr hWndTaskbar = User32.FindWindow("Shell_TrayWnd", null);
            VDesktopManager vdm = VDesktopManager.Get();
            UDictionary<IntPtr, VDesktopWindow> ignoredWindows = vdm.GetIgnoredWindows();
            ignoredWindows.Update(form.Handle, new VDesktopWindow(form.Handle));
            ignoredWindows.Update(hWndTaskbar, new VDesktopWindow(hWndTaskbar));

            DoubleBuffered = true;

            UpdateHotKeys();
            HotKeyManager.Get().HotKeyDown += KeyShowDesktopsEvent;
            HotKeyManager.Get().HotKeyPressed += KeyMoveDesktopsEvent;
            HotKeyManager.Get().HotKeyUp += KeyHideDesktopsEvent;
        }

        private void AddEvent(object o, EventArgs e)
        {
            AddDesktop();
            Invalidate();
        }

        private void RemoveEvent(object o, EventArgs e)
        {
            int ID = IdentifyDesktop(new Point(mouseX, mouseY));
            if (ID >= 1 && ID <= VDesktopManager.Get().GetDesktops().Count)
            {
                RemoveDesktop(ID);
            }
            else
            {
                //couldn't identify desktop
                //at last mouseposition
                //remove last desktop
                RemoveDesktop(VDesktopManager.Get().GetDesktops().Count);
            }
            Invalidate();
        }

        private void ShowSettingsEvent(object o, EventArgs e)
        {
            VDesktopForm form = VDesktopForm.Get();
            form.Invoke(new Action(() =>
            {
                form.ShowSettings();
            }));
        }

        public int GetPreviewWidth()
        {
            return previewWidth;
        }

        public int GetPreviewHeight()
        {
            return previewHeight;
        }

        private void SetPreviewHeight(int height)
        {
            float deskWidth = SystemInformation.VirtualScreen.Width;
            float deskHeight = SystemInformation.VirtualScreen.Height;
            float previewContentHeight = height - 2 * previewMargin - 3;
            float previewContentWidth = previewContentHeight * deskWidth / deskHeight;
            previewWidth = (int)(previewContentWidth + 2 * previewMargin);
            previewHeight = (int)(previewContentHeight + 2 * previewMargin + 3);
        }

        public void SetEditPreviewHeight(bool edit)
        {
            settingPreviewDimensions = edit;
        }

        private void ResizePreview(object sender, System.EventArgs e)
        {
            if (!settingPreviewDimensions)
            {
                //user is not supposed to change the preview dimensions
                //hence nothing to do
                return;
            }
            Control control = (Control)sender;
            SetPreviewHeight(control.Bounds.Height);
            Update();
            Invalidate();
        }

        public void UpdateHotKeys()
        {
            HotKeyManager.Get().UnregisterHotKeys();
            foreach (HotKey k in Settings.Get().GetKeyLeft())
            {
                HotKeyManager.Get().RegisterHotKey(k.key, k.keyModifier);
            }
            foreach (HotKey k in Settings.Get().GetKeyRight())
            {
                HotKeyManager.Get().RegisterHotKey(k.key, k.keyModifier);
            }
            foreach (HotKey k in Settings.Get().GetKeyMoveLeft())
            {
                HotKeyManager.Get().RegisterHotKey(k.key, k.keyModifier);
            }
            foreach (HotKey k in Settings.Get().GetKeyMoveRight())
            {
                HotKeyManager.Get().RegisterHotKey(k.key, k.keyModifier);
            }
            foreach (HotKey k in Settings.Get().GetKeyShow())
            {
                HotKeyManager.Get().RegisterHotKey(k.key, k.keyModifier);
            }
        }

        public void SetUseAero(bool use)
        {
            useAero = use;
            UpdateAero();
        }

        public void SetUseAddCloseButtons(bool use)
        {
            bool oldUse = useAddCloseButtons;
            useAddCloseButtons = use;
            if (use && !oldUse)
            {
                Update();
                Left -= 19;
            }
            else if (!use && oldUse)
            {
                Left += 19;
                Update();
            }
        }

        public void SetUseBorders(bool use)
        {
            useBorders = use;
            Invalidate();
        }

        public void SetUseTooltip(bool use)
        {
            useTooltip = use;
        }

        public void SetThumbnailView(ThumbnailView view)
        {
            windowPainter.Clear();
            switch (view)
            {
                case ThumbnailView.CachedAeroWindows:
                    windowPainter = cachedAeroPainter;
                    break;
                case ThumbnailView.AeroWindows:
                    windowPainter = aeroPainter;
                    break;
                case ThumbnailView.BasicWindows:
                    windowPainter = basicPainter;
                    break;
                default:
                    windowPainter = cachedAeroPainter;
                    break;
            }
        }

        public void SetUpdateThumbnailRate(double rate)
        {
            cachedAeroPainter.SetUpdateThumbnailRate(rate);
        }

        public void SetEmphasizeDesktop(EmphasizeDesktop eDesktop)
        {
            switch (eDesktop)
            {
                case EmphasizeDesktop.BorderedEmphasizer:
                    desktopEmphasizer = borderedEmphasizer;
                    break;
                case EmphasizeDesktop.SmoothEmphasizer:
                    desktopEmphasizer = smoothEmphasizer;
                    break;
                case EmphasizeDesktop.NoEmphasizer:
                    desktopEmphasizer = noEmphasizer;
                    break;
                default:
                    desktopEmphasizer = borderedEmphasizer;
                    break;
            }
        }

        private void KeyShowDesktopsEvent(object o, HotKeyEventArgs e)
        {
            bool bShow = false;
            foreach (HotKey hkey in Settings.Get().GetKeyShow())
            {
                if (e.keyModifier == hkey.keyModifier && hkey.key == e.key)
                {
                    bShow = true;
                }
            }

            if (bShow)
            {
                showingByKeyboard = true;
                Point p = System.Windows.Forms.Cursor.Position;
                DesktopsForm overview = this;
                overview.Invoke(new Action(() => { overview.Present(lastPresentPoint); }));
            }
        }

        private void KeyMoveDesktopsEvent(object o, HotKeyEventArgs e)
        {
            bool bLeft = false;
            bool bRight = false;
            bool bMoveLeft = false;
            bool bMoveRight = false;
            foreach (HotKey hkey in Settings.Get().GetKeyLeft())
            {
                if (e.keyModifier== hkey.keyModifier && hkey.key == e.key)
                {
                    bLeft = true;
                }
            }
            foreach (HotKey hkey in Settings.Get().GetKeyRight())
            {
                if (e.keyModifier == hkey.keyModifier && hkey.key == e.key)
                {
                    bRight = true;
                }
            }
            foreach (HotKey hkey in Settings.Get().GetKeyMoveLeft())
            {
                if (e.keyModifier == hkey.keyModifier && hkey.key == e.key)
                {
                    bMoveLeft = true;
                }
            }
            foreach (HotKey hkey in Settings.Get().GetKeyMoveRight())
            {
                if (e.keyModifier == hkey.keyModifier && hkey.key == e.key)
                {
                    bMoveRight = true;
                }
            }

            VDesktopManager manager = VDesktopManager.Get();
            windowPainter.LeaveDesktop(manager.GetCurrentDesktop());
            List<VDesktop> desktops = manager.GetDesktops();
            uint ID = manager.GetCurrentDesktop().getID();
            if (bLeft)
            {
                //move to left desktop
                if (desktops.Count() > 1)
                {
                    ID = 1 + (ID + (uint)desktops.Count()) % (uint)desktops.Count();

                    VDesktopForm form = VDesktopForm.Get();
                    List<DesktopTray> trays = form.GetDesktopTrays();
                    foreach (DesktopTray t in trays)
                    {
                        if (t.GetID() == ID)
                        {
                            t.Select();
                            Update();
                            Invalidate();
                        }
                    }
                }
            }
            else if (bRight)
            {
                //move to right desktop
                if (desktops.Count() > 1)
                {
                    ID = 1 + (ID - 2 + (uint)desktops.Count()) % (uint)desktops.Count();

                    VDesktopForm form = VDesktopForm.Get();
                    List<DesktopTray> trays = form.GetDesktopTrays();
                    foreach (DesktopTray t in trays)
                    {
                        if (t.GetID() == ID)
                        {
                            t.Select();
                            Update();
                            Invalidate();
                        }
                    }
                }
            }
            else if (bMoveLeft)
            {
                //move topmost window to the left desktop
                if (desktops.Count() > 1)
                {
                    VDesktop currDesk = manager.GetCurrentDesktop();
                    List<VDesktopWindow> windows = currDesk.GetOrderedWindows();
                    VDesktopWindow topWindow = null;
                    foreach (VDesktopWindow w in windows)
                    {
                        if (w.GetHandle() != VDesktopForm.Get().Handle)
                        {
                            //don't move the invisible VDesktopForm
                            topWindow = w;
                        }
                    }
                    if (topWindow != null)
                    {
                        topWindow.Hide();
                        uint newDeskID = 1 + (ID + (uint)desktops.Count()) % (uint)desktops.Count();

                        //before inserting window to desktop
                        //remove window from all other desktops
                        //get desktop with newDeskID
                        VDesktop desktop = null;
                        foreach (VDesktop d in desktops)
                        {
                            if (d.GetWindows().ContainsKey(topWindow.GetHandle()))
                            {
                                d.GetWindows().Remove(topWindow.GetHandle());
                            }
                            if (d.getID() == newDeskID)
                            {
                                desktop = d;
                            }
                        }
                        desktop.GetWindows().Update(topWindow.GetHandle(), topWindow);
                        Update();
                        Invalidate();

                        VDesktopForm form = VDesktopForm.Get();
                        List<DesktopTray> trays = form.GetDesktopTrays();
                        foreach (DesktopTray t in trays)
                        {
                            if (t.GetID() == ID)
                            {
                                t.Select();
                                Update();
                                Invalidate();
                            }
                        }
                    }
                }
            }
            else if (bMoveRight)
            {
                //move topmost window to the right desktop
                if (desktops.Count() > 1)
                {
                    VDesktop currDesk = manager.GetCurrentDesktop();
                    List<VDesktopWindow> windows = currDesk.GetOrderedWindows();
                    VDesktopWindow topWindow = null;
                    foreach (VDesktopWindow w in windows)
                    {
                        if (w.GetHandle() != VDesktopForm.Get().Handle)
                        {
                            //don't move the invisible VDesktopForm
                            topWindow = w;
                        }
                    }
                    if (topWindow != null)
                    {
                        topWindow.Hide();
                        uint newDeskID = 1 + (ID - 2 + (uint)desktops.Count()) % (uint)desktops.Count();

                        //before inserting window to desktop
                        //remove window from all other desktops
                        //get desktop with newDeskID
                        VDesktop desktop = null;
                        foreach (VDesktop d in desktops)
                        {
                            if (d.GetWindows().ContainsKey(topWindow.GetHandle()))
                            {
                                d.GetWindows().Remove(topWindow.GetHandle());
                            }
                            if (d.getID() == newDeskID)
                            {
                                desktop = d;
                            }
                        }
                        desktop.GetWindows().Update(topWindow.GetHandle(), topWindow);
                        Update();
                        Invalidate();

                        VDesktopForm form = VDesktopForm.Get();
                        List<DesktopTray> trays = form.GetDesktopTrays();
                        foreach (DesktopTray t in trays)
                        {
                            if (t.GetID() == ID)
                            {
                                t.Select();
                                Update();
                                Invalidate();
                            }
                        }
                    }
                }
            }
        }

        private void KeyHideDesktopsEvent(object o, HotKeyEventArgs e)
        {
            bool bShow = false;
            foreach (HotKey hkey in Settings.Get().GetKeyShow())
            {
                if (e.keyModifier == hkey.keyModifier && hkey.key == e.key)
                {
                    bShow = true;
                }
            }

            if (bShow)
            {
                showingByKeyboard = false;
                DesktopsForm overview = this;
                overview.Invoke(new Action(() => { overview.RequestStopPresenting(); }));
            }
        }

        public bool isShowingByKeyboard()
        {
            return showingByKeyboard;
        }

        public void UpdateAero()
        {
            if (useAero)
            {
                try
                {
                    IntPtr mainWindowPtr = this.Handle;
                    BackColor = Color.FromArgb(255, 0, 0, 0);

                    MARGINS margins = new MARGINS();
                    margins.cxLeftWidth = Convert.ToInt32(2000);
                    margins.cxRightWidth = Convert.ToInt32(2000);
                    margins.cyTopHeight = Convert.ToInt32(2000);
                    margins.cyBottomHeight = Convert.ToInt32(2000);

                    int hr = Dwmapi.DwmExtendFrameIntoClientArea(mainWindowPtr, ref margins);
                    if (hr < 0)
                    {
                        BackColor = Color.FromArgb(255, 255, 255, 255);
                    }
                }
                catch (Exception)
                {
                    BackColor = Color.FromArgb(255, 255, 255, 255);
                }
            }
            else
            {
                try
                {
                    IntPtr mainWindowPtr = this.Handle;
                    BackColor = Color.FromArgb(255, 0, 0, 0);

                    MARGINS margins = new MARGINS();
                    margins.cxLeftWidth = Convert.ToInt32(0);
                    margins.cxRightWidth = Convert.ToInt32(0);
                    margins.cyTopHeight = Convert.ToInt32(0);
                    margins.cyBottomHeight = Convert.ToInt32(0);

                    int hr = Dwmapi.DwmExtendFrameIntoClientArea(mainWindowPtr, ref margins);
                    if (hr < 0)
                    {
                        BackColor = Color.FromArgb(255, 255, 255, 255);
                    }
                }
                catch (Exception)
                {
                }
                BackColor = Color.FromArgb(255, 255, 255, 255);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            VDesktopManager vdm = VDesktopManager.Get();
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            //emphasize active desktop
            desktopEmphasizer.EmphasizeActiveDesktop(g, previewWidth, previewHeight);

            //draw separating lines
            int currentDesktopCounter = 0;
            if (useBorders)
            {
                currentDesktopCounter = vdm.GetDesktops().Count;
                LinearGradientBrush separatorBrush1 = new LinearGradientBrush(new Point(0, 10), new Point(0, previewHeight - 20), Color.FromArgb(0, 255, 255, 255), Color.FromArgb(255, 255, 255, 255));
                ColorBlend separatorBlend1 = new ColorBlend();
                separatorBlend1.Colors = new Color[] { Color.FromArgb(0, 255, 255, 255), Color.FromArgb(255, 255, 255, 255), Color.FromArgb(0, 255, 255, 255) };
                separatorBlend1.Positions = new float[] { 0, 0.5f, 1 };
                separatorBrush1.InterpolationColors = separatorBlend1;
                Pen separatorPen1 = new Pen(separatorBrush1);
                LinearGradientBrush separatorBrush2 = new LinearGradientBrush(new Point(previewWidth * currentDesktopCounter, 10), new Point(previewWidth * currentDesktopCounter, previewHeight - 20), Color.FromArgb(0, 255, 255, 255), Color.FromArgb(255, 255, 255, 255));
                ColorBlend separatorBlend2 = new ColorBlend();
                separatorBlend2.Colors = new Color[] { Color.FromArgb(0, 0, 0, 0), Color.FromArgb(255, 0, 0, 0), Color.FromArgb(0, 0, 0, 0) };
                separatorBlend2.Positions = new float[] { 0, 0.5f, 1 };
                separatorBrush2.InterpolationColors = separatorBlend2;
                Pen separatorPen2 = new Pen(separatorBrush2);
                foreach (VDesktop desktop in vdm.GetDesktops())
                {
                    currentDesktopCounter--;
                    if (currentDesktopCounter > 0)
                    {
                        g.DrawLine(separatorPen1, new Point(previewWidth * currentDesktopCounter, 10), new Point(previewWidth * currentDesktopCounter, previewHeight - 20));
                        g.DrawLine(separatorPen2, new Point(previewWidth * currentDesktopCounter - 1, 10), new Point(previewWidth * currentDesktopCounter - 1, previewHeight - 20));
                    }
                }
                separatorPen1.Dispose();
                separatorBrush1.Dispose();
                separatorPen2.Dispose();
                separatorBrush2.Dispose();
            }

            //execute clear operation
            //if nothing is dragged, or clear is required
            //according to windowPainter
            if (!isDragging || dragSelection == null || windowPainter.isClearNecessary())
            {
                windowPainter.Clear();
            }

            //draw desktops
            currentDesktopCounter = vdm.GetDesktops().Count;
            foreach (VDesktop desktop in vdm.GetDesktops())
            {
                currentDesktopCounter--;
                try
                {
                    windowPainter.paintDesktop(g, isDragging && dragSelection != null, desktop, previewWidth * currentDesktopCounter + previewMargin, previewMargin, previewWidth - previewMargin * 2, previewHeight - previewMargin * 2 - 3);
                }
                catch (Exception)
                {
                    //advanced drawing not possible
                    //stick to basic window drawing
                    basicPainter.paintDesktop(g, isDragging && dragSelection != null, desktop, previewWidth * currentDesktopCounter + previewMargin, previewMargin, previewWidth - previewMargin * 2, previewHeight - previewMargin * 2 - 3);
                }
            }

            //draw drag selection, if anything is dragged
            if (isDragging && dragSelection != null)
            {
                Point windowPos = new Point(dragMousePos.X - relativeDragMousePos.X, dragMousePos.Y - relativeDragMousePos.Y);
                try
                {
                    windowPainter.paintWindow(g, false, 0, 0, previewWidth - previewMargin * 2, previewHeight - previewMargin * 2 - 3, dragSelection, true, windowPos);
                }
                catch (Exception)
                {
                    //advanced drawing not possible
                    //stick to basic window drawing
                    basicPainter.paintWindow(g, false, 0, 0, previewWidth - previewMargin * 2, previewHeight - previewMargin * 2 - 3, dragSelection, true, windowPos);
                }
            }

            if (useAddCloseButtons)
            {
                //draw closebutton
                if (hoveredDesktop >= 1 && hoveredDesktop <= vdm.GetDesktops().Count)
                {
                    int closeLocX = previewWidth * (vdm.GetDesktops().Count + 1 - hoveredDesktop) - 19;
                    int closeLocY = 3;
                    Rectangle closeButtonRect = new Rectangle(closeLocX, closeLocY, 16, 16);
                    Icon closeButtonIcon = null;
                    if (hoveringClose)
                    {
                        if (clickingClose)
                        {
                            closeButtonIcon = closeButtonIcon_clicked;
                        }
                        else
                        {
                            closeButtonIcon = closeButtonIcon_focused;
                        }
                    }
                    else
                    {
                        closeButtonIcon = closeButtonIcon_unfocused;
                    }
                    g.DrawIcon(closeButtonIcon, closeButtonRect);
                }

                //draw addbutton
                int addLocX = previewWidth * vdm.GetDesktops().Count + 1;
                int addLocY = 3;
                Rectangle addButtonRect = new Rectangle(addLocX, addLocY, 16, 16);
                Icon addButtonIcon = null;
                if (hoveringAdd)
                {
                    if (clickingAdd)
                    {
                        addButtonIcon = addButtonIcon_clicked;
                    }
                    else
                    {
                        addButtonIcon = addButtonIcon_focused;
                    }
                }
                else
                {
                    addButtonIcon = addButtonIcon_unfocused;
                }
                g.DrawIcon(addButtonIcon, addButtonRect);
            }

        }

        public static Rectangle ClipDesktopWindow(Rectangle wRectangle)
        {
            int dX = SystemInformation.VirtualScreen.Width - 1;
            int dY = SystemInformation.VirtualScreen.Height - 1;

            int left = Math.Min(Math.Max(wRectangle.Left, 0), dX);
            int right = Math.Min(Math.Max(wRectangle.Right, 0), dX);
            int bottom = Math.Min(Math.Max(wRectangle.Bottom, 0), dY);
            int top = Math.Min(Math.Max(wRectangle.Top, 0), dY);

            if (left >= right || top >= bottom)
            {
                //clip complete rectangle
                return new Rectangle(0, 0, 0, 0);
            }

            return new Rectangle(left, top, right-left, bottom-top);
        }

        public static Rectangle Desktop2Preview(int previewX, int previewY, int previewWidth, int previewHeight, Rectangle wRectangle)
        {
            if (wRectangle.Bottom == 0 && wRectangle.Top == 0 && wRectangle.Left == 0 && wRectangle.Right == 0)
            {
                //don' transform clipped rectangles
                return new Rectangle(0, 0, 0, 0);
            }

            float dw = SystemInformation.VirtualScreen.Width;
            float dh = SystemInformation.VirtualScreen.Height;
            float pw = previewWidth;
            float ph = previewHeight;
            float px = previewX;
            float py = previewY;

            float p1_x = ((float)wRectangle.Left) * (pw/dw) + px;
            float p1_y = ((float)wRectangle.Top) * (ph/dh) + py;
            float p2_x = ((float)wRectangle.Right) * (pw/dw) + px;
            float p2_y = ((float)wRectangle.Bottom) * (ph/dh) + py;

            int left = (int)Math.Floor(p1_x);
            int right = (int)Math.Floor(p2_x);
            int top = (int)Math.Floor(p1_y);
            int bottom = (int)Math.Floor(p2_y);

            return new Rectangle(left, top, right - left, bottom - top);
        }

        public void StopPresenting()
        {
            if (Visible)
            {
                TopMost = false;
                Hide();
            }
        }

        public void RequestStopPresenting()
        {
            VDesktopWindow currentWindow = new VDesktopWindow(this.Handle);
            Rectangle wRect = currentWindow.GetRectangle();
            Point mouse = System.Windows.Forms.Cursor.Position;
            bool mouseIsInside = wRect.Left <= mouse.X && wRect.Top <= mouse.Y && wRect.Right >= mouse.X && wRect.Bottom >= mouse.Y;
            if (!mouseIsInside)
            {
                StopPresenting();
            }
        }

        public void Present()
        {
            UpdateAero();
            Update();
            Show();
            WindowState = FormWindowState.Normal;
            TopMost = true;
        }

        public void Present(Point center)
        {
            lastPresentPoint = center;
            UpdateAero();
            Update();
            UpdatePosition(center);
            Show();
            WindowState = FormWindowState.Normal;
            TopMost = true;
        }

        public new void Update()
        {
            base.Update();

            //get desktop rectangle minus taskbar
            int addButtonSize = 0;
            if (useAddCloseButtons)
            {
                addButtonSize = 19;
            }
            VDesktopManager vdm = VDesktopManager.Get();
            this.Size = new Size(17 + previewWidth * vdm.GetDesktops().Count + addButtonSize, previewHeight);
            vdm.GetCurrentDesktop().UpdateWindows();
        }

        public void UpdatePosition(Point center)
        {
            //get rectangle of desktop area without taskbar
            int left = (int)System.Windows.SystemParameters.WorkArea.Left;
            int right = (int)System.Windows.SystemParameters.WorkArea.Right;
            int top = (int)System.Windows.SystemParameters.WorkArea.Top;
            int bottom = (int)System.Windows.SystemParameters.WorkArea.Bottom;
            Rectangle rect = new Rectangle(left + 10, top + 10, right - left - 20, bottom - top - 20);

            //get rectangle with the center from the parameter
            Rectangle cRect = new Rectangle(Size.Width / 2 + center.X, Size.Height / 2 + center.Y, Size.Width, Size.Height);

            //find a left upper corner such that the
            //form lies inside rect
            Point lu = new Point(cRect.Left, cRect.Top);
            if (cRect.Left < rect.Left)
            {
                lu.X = rect.Left;
            }
            else if (cRect.Right > rect.Right)
            {
                lu.X = rect.Right - Size.Width;
            }
            if (cRect.Top < rect.Top)
            {
                lu.Y = rect.Top;
            }
            else if (cRect.Bottom > rect.Bottom)
            {
                lu.Y = rect.Bottom - Size.Height;
            }
            this.Left = lu.X;
            this.Top = lu.Y;
        }

        public void AddDesktop()
        {
            VDesktopForm form = VDesktopForm.Get();
            form.Invoke(new Action(() =>
            {
                form.AddDesktop();
            }));
            Update();
            this.Left -= previewWidth;
        }

        public void RemoveDesktop(int ID)
        {
            VDesktopManager vdm = VDesktopManager.Get();
            if (ID < 1 || ID > vdm.GetDesktops().Count)
            {
                //non-existing desktop id, nothing to do
                return;
            }

            //shift desktops >= ID to the right
            if (vdm.GetDesktops().Count > 1)
            {
                int currDeskID = (int)vdm.GetCurrentDesktop().getID();
                for (int i = Math.Max(1, ID-1); i < vdm.GetDesktops().Count ; i++)
                {
                    UDictionary<IntPtr, VDesktopWindow> windows = vdm.GetDesktops()[i].GetWindows();
                    foreach (IntPtr hWnd in windows.Keys)
                    {
                        vdm.GetDesktops()[i - 1].GetWindows()[hWnd] = windows[hWnd];
                        if (i == currDeskID)
                        {
                            windows[hWnd].Show();
                        }
                    }
                    vdm.GetDesktops()[i].GetWindows().Clear();
                }

                if (vdm.GetCurrentDesktop().getID() >= (uint)ID && vdm.GetCurrentDesktop().getID() > 1)
                {
                    //current desktop belongs to the moved desktops
                    //change current desktop
                    int newCurrentID = (int)vdm.GetCurrentDesktop().getID() - 1;
                    SelectDesktop(newCurrentID);
                }
            }

            //removing highest desktop?
            //don't close overview on mouseleave this time!
            if (ID == vdm.GetDesktops().Count && ID > 1)
            {
                closedGreatestDesktop = true;
            }

            //remove desktop with the highest ID
            int oldDesktopCount = vdm.GetDesktops().Count;
            VDesktopForm form = VDesktopForm.Get();
            form.Invoke(new Action(() =>
                {
                    form.RemoveDesktop();
                }));
            if (oldDesktopCount > 1)
            {
                this.Left += previewWidth;
            }
            Update();
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (e.Button == MouseButtons.Left)
            {
                leftMouseIsDown = false;
                if (((!useAddCloseButtons) || (!clickingClose)) && e.X == leftMouseX && e.Y == leftMouseY)
                {
                    SelectDesktop(new Point(e.X, e.Y));
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                rightMouseIsDown = false;
            }

            if (isDragging)
            {
                //drop drag selection
                DropDragSelection(new Point(e.X, e.Y));
                isDragging = false;
            }

            if (useAddCloseButtons)
            {
                //closebutton
                if (clickingClose)
                {
                    //change state of closebutton
                    clickingClose = false;
                    Invalidate();

                    if (hoveringClose)
                    {
                        //clicked on closebutton
                        //close clicked desktop
                        int closeDesktopID = IdentifyDesktop(new Point(e.X, e.Y));
                        if (closeDesktopID >= 1 && closeDesktopID <= VDesktopManager.Get().GetDesktops().Count)
                        {
                            RemoveDesktop(closeDesktopID);
                        }
                    }
                }
                clickingClose = false;

                //addbutton
                if (clickingAdd)
                {
                    //change state of addbutton
                    clickingAdd = false;
                    Invalidate();

                    if (hoveringAdd)
                    {
                        //clicked on addbutton
                        //add new desktop
                        AddDesktop();
                    }
                }
            }
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);

            if (ClientRectangle.Contains(PointToClient(Control.MousePosition)))
            {
                //mousepointer is still above form
                //... may hover over tooltip
                //hence don't process this mouseleave event
                return;
            }
            if (closedGreatestDesktop)
            {
                //removing highest desktop?
                //don't close overview on mouseleave this time!
                closedGreatestDesktop = false;
                return;
            }

            isDragging = leftMouseIsDown = rightMouseIsDown = false;
            if ((!isShowingByKeyboard()) && (!settingPreviewDimensions))
            {
                StopPresenting();
            }

            //closebutton
            hoveredDesktop = -1;
            Invalidate();
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            lastMouseUpdate = System.DateTime.Now.ToUniversalTime().Ticks;
            mouseX = e.X;
            mouseY = e.Y;
            System.Threading.Timer timer = new System.Threading.Timer(ShowTooltipEvent, null, 500, 0);
            if (leftMouseIsDown && !isDragging)
            {
                //initiate window drag
                StartDrag(new Point(e.X, e.Y));
                isDragging = true;
            }
            if (isDragging && dragSelection != null)
            {
                dragMousePos = new Point(e.X, e.Y);
                Invalidate();
            }
            showTooltip = !isDragging;

            if (useAddCloseButtons)
            {
                //closebutton
                int deskID = IdentifyDesktop(new Point(mouseX, mouseY));
                int newHoveredDesktop = -1;
                if (deskID >= 1 && deskID <= VDesktopManager.Get().GetDesktops().Count)
                {
                    newHoveredDesktop = deskID;
                }
                int locX = previewWidth * (VDesktopManager.Get().GetDesktops().Count + 1 - hoveredDesktop) - 19;
                int locY = 0;
                bool newHoveringClose = false;
                if (mouseX >= locX && mouseX <= locX + 19 && mouseY >= locY && mouseY <= locY + 19)
                {
                    newHoveringClose = true;
                }
                if (newHoveredDesktop != hoveredDesktop || newHoveringClose != hoveringClose)
                {
                    hoveringClose = newHoveringClose;
                    hoveredDesktop = newHoveredDesktop;
                    Invalidate();
                }

                //addbutton
                locX = previewWidth * VDesktopManager.Get().GetDesktops().Count;
                locY = 0;
                bool newHoveringAdd = false;
                if (mouseX >= locX && mouseX <= locX + 19 && mouseY >= locY && mouseY <= locY + 19)
                {
                    newHoveringAdd = true;
                }
                if (newHoveringAdd != hoveringAdd)
                {
                    hoveringAdd = newHoveringAdd;
                    Invalidate();
                }
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left)
            {
                leftMouseX = e.X;
                leftMouseY = e.Y;
                leftMouseIsDown = true;
            }
            else if (e.Button == MouseButtons.Right)
            {
                rightMouseX = e.X;
                rightMouseY = e.Y;
                rightMouseIsDown = true;
            }

            if (useAddCloseButtons)
            {
                //closebutton
                if (hoveringClose)
                {
                    clickingClose = true;
                    Invalidate();
                }

                //addbutton
                if (hoveringAdd)
                {
                    clickingAdd = true;
                    Invalidate();
                }
            }
        }

        public int IdentifyDesktop(Point coordinate)
        {
            VDesktopManager vdm = VDesktopManager.Get();
            int index = (int)Math.Floor((float)coordinate.X / (float)previewWidth);
            index = vdm.GetDesktops().Count - index;
            return index;
        }

        public void SelectDesktop(Point coordinate)
        {
            SelectDesktop(IdentifyDesktop(coordinate));
        }

        public void SelectDesktop(int ID)
        {
            VDesktopManager manager = VDesktopManager.Get();
            windowPainter.LeaveDesktop(manager.GetCurrentDesktop());

            VDesktopForm form = VDesktopForm.Get();
            List<DesktopTray> trays = form.GetDesktopTrays();
            foreach (DesktopTray t in trays)
            {
                if (t.GetID() == ID)
                {
                    t.Select();
                    Update();
                    Invalidate();
                }
            }
        }

        public VDesktopWindow IdentifyWindow(int desktopID, Point coordinate)
        {
            VDesktopManager vdm = VDesktopManager.Get();
            List<VDesktop> desktops = vdm.GetDesktops();
            VDesktop desktop = null;
            foreach (VDesktop d in desktops)
            {
                if (d.getID() == desktopID)
                {
                    desktop = d;
                }
            }
            if (desktop == null)
            {
                return null;
            }
            List<VDesktopWindow> windows = desktop.GetOrderedWindows();
            VDesktopWindow window = null;
            foreach (VDesktopWindow w in windows)
            {
                if(!w.IsValid())
                {
                    continue;
                }
                Rectangle ntr = w.GetRectangle();
                Rectangle r = Desktop2Preview(previewWidth * (desktops.Count - desktopID) + previewMargin, previewMargin, previewWidth - previewMargin * 2, previewHeight - previewMargin * 2 - 3, ntr);
                if (
                    r.Left <= coordinate.X && r.Top <= coordinate.Y &&
                    r.Right >= coordinate.X && r.Bottom >= coordinate.Y
                    )
                {
                    //mouse is inside
                    //the current window
                    //consider this window for a selection
                    window = w;
                }
            }

            return window;
        }

        private void ShowTooltipEvent(object o)
        {
            if (!useTooltip)
            {
                return;
            }
            long time = (System.DateTime.Now.ToUniversalTime().Ticks - lastMouseUpdate) / 10000;
            if (time > 200 && showTooltip && !isDragging)
            {
                Invoke(new Action(() => {
                    if (toolTip == null)
                    {
                        toolTip = new ToolTip();
                    }
                    int DeskID = IdentifyDesktop(new Point(mouseX, mouseY));
                    VDesktopWindow window = IdentifyWindow(DeskID, new Point(mouseX, mouseY));
                    if (window != null && window.IsValid())
                    {
                        toolTip.SetToolTip(this, window.GetTitle());
                    }
                    else if(DeskID > 0)
                    {
                        toolTip.SetToolTip(this, "Desktop " + DeskID);
                    }
                }));
            }
            if(isDragging)
            {
                Invoke(new Action(() => {
                    if (toolTip != null)
                    {
                        toolTip.Hide(this);
                    }
                    lastMouseUpdate = System.DateTime.Now.ToUniversalTime().Ticks;
                }));
            }
        }

        public void StartDrag(Point coordinate)
        {
            dragFromDesktop = IdentifyDesktop(coordinate);
            dragSelection = IdentifyWindow(dragFromDesktop, coordinate);
            if (dragSelection != null)
            {
                //dragging a window, hence calculate mouse drag position
                VDesktopManager vdm = VDesktopManager.Get();
                List<VDesktop> desktops = vdm.GetDesktops();
                Rectangle ntr = dragSelection.GetRectangle();
                Rectangle r = Desktop2Preview(previewWidth * (desktops.Count - dragFromDesktop) + previewMargin, previewMargin, previewWidth - previewMargin * 2, previewHeight - previewMargin * 2 - 3, ntr);
                relativeDragMousePos = new Point(coordinate.X - r.Left, coordinate.Y - r.Top);
            }
        }

        public void DropDragSelection(Point coordinate)
        {
            if (dragSelection == null || !dragSelection.IsValid())
            {
                //no drag selection
                //hence nothing to do
                return;
            }
            VDesktopManager vdm = VDesktopManager.Get();
            List<VDesktop> desktops = vdm.GetDesktops();
            int dropDesk = IdentifyDesktop(coordinate);
            VDesktop desktop = null;
            foreach (VDesktop d in desktops)
            {
                if (d.getID() == dropDesk)
                {
                    desktop = d;
                }
            }
            if (desktop == null || desktop.getID() == dragFromDesktop)
            {
                //invalid desktop
                //or drop at desktop
                //from which the window was dragged
                dragSelection = null;
                Invalidate();
                //repaint preview area without drag selection
                return;
            }
            if (vdm.GetCurrentDesktop() != null && vdm.GetCurrentDesktop().getID() == dropDesk)
            {
                dragSelection.Show();
            }
            else
            {
                dragSelection.Hide();
            }
            //before inserting window to desktop
            //remove window from all other desktops
            foreach (VDesktop d in desktops)
            {
                if (d.GetWindows().ContainsKey(dragSelection.GetHandle()))
                {
                    d.GetWindows().Remove(dragSelection.GetHandle());
                }
            }
            desktop.GetWindows().Update(dragSelection.GetHandle(), dragSelection);
            dragSelection = null;

            Update();
            Invalidate();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                const int WS_THICKFRAME = 0x40000;
                const int WS_BORDER = ~0x800000;
                CreateParams p = base.CreateParams;
                p.Style |= WS_THICKFRAME;     // WS_THICKFRAME
                p.Style &= WS_BORDER;   // WS_BORDER
                return p;
            }
        }

    }

    public interface DesktopEmphasizer
    {
        void EmphasizeActiveDesktop(Graphics g, int previewWidth, int previewHeight);
    }

    public class SmoothEmphasizer : DesktopEmphasizer
    {
        private Bitmap previewBackground = null;

        public void EmphasizeActiveDesktop(Graphics g, int previewWidth, int previewHeight)
        {
            VDesktopManager vdm = VDesktopManager.Get();

            if (previewBackground == null)
            {
                Bitmap emphasizeImg = new Bitmap(previewWidth, previewHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                Graphics ig = Graphics.FromImage(emphasizeImg);
                GraphicsPath ipath = new GraphicsPath();
                ipath.AddRectangle(new Rectangle(0, 0, previewWidth, previewHeight - 20));
                PathGradientBrush ibrush = new PathGradientBrush(ipath);
                ColorBlend iblend = new ColorBlend();
                iblend.Positions = new float[] { 0.0f, 0.1f, 0.5f, 1 };
                iblend.Colors = new Color[] { Color.FromArgb(0, 184, 219, 253), Color.FromArgb(40, 184, 219, 253), Color.FromArgb(60, 184, 219, 253), Color.FromArgb(70, 184, 219, 253) };
                ibrush.InterpolationColors = iblend;
                ig.Clear(Color.FromArgb(0, 184, 219, 253));
                ig.FillRectangle(ibrush, 0, 0, previewWidth, previewHeight - 20);
                ibrush.Dispose();
                ipath.Dispose();

                previewBackground = VGraphics.BlurImage(emphasizeImg, 15);
                emphasizeImg.Dispose();
            }
            g.DrawImage(previewBackground, 0 + previewWidth * (vdm.GetDesktops().Count - vdm.GetCurrentDesktop().getID()), 0);
        }

    }

    public class BorderedEmphasizer: DesktopEmphasizer
    {

        public void EmphasizeActiveDesktop(Graphics g, int previewWidth, int previewHeight)
        {
            VDesktopManager vdm = VDesktopManager.Get();

            int pX = (int)(3 + (previewWidth) * (vdm.GetDesktops().Count - vdm.GetCurrentDesktop().getID()));
            int pY = 0;
            int w = previewWidth - 6;
            int h = previewHeight - 17;
            Rectangle deskRectangle = new Rectangle(pX, pY, w, h);
            int deskRound = 5;

            GraphicsPath deskPath = new GraphicsPath();
            deskPath.AddArc(deskRectangle.X, deskRectangle.Y, deskRound, deskRound, 180, 90);
            deskPath.AddArc(deskRectangle.X + deskRectangle.Width - deskRound, deskRectangle.Y, deskRound, deskRound, 270, 90);
            deskPath.AddArc(deskRectangle.X + deskRectangle.Width - deskRound, deskRectangle.Y + deskRectangle.Height - deskRound, deskRound, deskRound, 0, 90);
            deskPath.AddArc(deskRectangle.X, deskRectangle.Y + deskRectangle.Height - deskRound, deskRound, deskRound, 90, 90);
            deskPath.CloseAllFigures();

            LinearGradientBrush deskBrush = new LinearGradientBrush(deskRectangle, Color.FromArgb(255, 255, 255, 255), Color.FromArgb(255, 255, 255, 255), 90);
            ColorBlend deskBlend = new ColorBlend();
            deskBlend.Colors = new Color[] { Color.FromArgb(150, 125, 159, 199), Color.FromArgb(150, 147, 185, 229) };
            deskBlend.Positions = new float[] { 0, 1 };
            deskBrush.InterpolationColors = deskBlend;
            g.FillPath(deskBrush, deskPath);

            Pen deskPen = new Pen(Color.FromArgb(150, 206, 220, 235));
            g.DrawPath(deskPen, deskPath);

            deskPath.Dispose();
            deskPen.Dispose();
            deskBrush.Dispose();
        }

    }

    public class NoEmphasizer : DesktopEmphasizer
    {

        public void EmphasizeActiveDesktop(Graphics g, int previewWidth, int previewHeight)
        {
        }

    }

    public interface WindowPainter
    {
        void paintDesktop(Graphics g, bool isDraggingWindow, VDesktop desktop, int x, int y, int width, int height);
        void paintWindow(Graphics g, bool isDraggingDifferentWindow, int x, int y, int width, int height, VDesktopWindow window, bool useDifferentPosition, Point differentPosition);
        void LeaveDesktop(VDesktop desktop);
        void Clear();
        bool isClearNecessary();
    }

    public class BasicPainter: WindowPainter
    {
        public void paintDesktop(Graphics g, bool isDraggingWindow, VDesktop desktop, int x, int y, int width, int height)
        {
            List<VDesktopWindow> windows = desktop.GetOrderedWindows();
            foreach (VDesktopWindow window in windows)
            {
                if (!window.IsValid())
                {
                    //this window does not
                    //exist anymore despite
                    //of being listed as a window
                    //of this desktop
                    continue;
                }
                paintWindow(g, isDraggingWindow, x, y, width, height, window);
            }
        }

        private void paintWindow(Graphics g, bool isDraggingWindow, int x, int y, int width, int height, VDesktopWindow window)
        {
            paintWindow(g, isDraggingWindow, x, y, width, height, window, false, new Point());
        }

        public void paintWindow(Graphics g, bool isDraggingDifferentWindow, int x, int y, int width, int height, VDesktopWindow window, bool useDifferentPosition, Point differentPosition)
        {
            if (!window.IsValid())
            {
                //this window does not
                //exist anymore despite
                //of being listed as a window
                //of this desktop
                return;
            }
            Rectangle wRectangle = window.GetRectangle();
            Rectangle rectangle = DesktopsForm.Desktop2Preview(x, y, width, height, DesktopsForm.ClipDesktopWindow(wRectangle));
            if (rectangle.Bottom == 0 && rectangle.Top == 0 && rectangle.Width == 0 && rectangle.Height == 0)
            {
                //a clipped rectangle, don't paint it
                return;
            }
            if (useDifferentPosition)
            {
                //use differentPosition as the
                //upper left corner of the preview
                //of the window
                rectangle = new Rectangle(differentPosition.X, differentPosition.Y, rectangle.Width, rectangle.Height);
            }

            //paint window rectangle
            LinearGradientBrush windowBrush = new LinearGradientBrush(rectangle, Color.FromArgb(255, 255, 255, 255), Color.FromArgb(255, 255, 255, 255), 30);
            ColorBlend windowBlend = new ColorBlend();
            windowBlend.Colors = new Color[] { Color.FromArgb(255, 255, 255, 255), Color.FromArgb(255, 184, 219, 253) };
            windowBlend.Positions = new float[] { 0, 1 };
            windowBrush.InterpolationColors = windowBlend;
            g.FillRectangle(windowBrush, rectangle);
            windowBrush.Dispose();

            //paint window borders
            paintWindowBorder(g, rectangle, Color.FromArgb(0, 0, 0, 0), Color.FromArgb(255, 0, 0, 0));

            //paint window icon, if it exists
            Icon icon = null;
            if ((icon = window.GetIcon()) != null)
            {
                try
                {
                    Rectangle iconRect = new Rectangle(rectangle.Left, rectangle.Top, 16, 16);
                    g.DrawIcon(icon, iconRect);
                    icon.Dispose();
                }
                catch (Exception e)
                {
                }
            }
        }

        private void paintWindowBorder(Graphics g, Rectangle rectangle, Color transparent, Color solid)
        {
            Point p_left_top = new Point(rectangle.Left, rectangle.Top);
            Point p_left_bottom = new Point(rectangle.Left, rectangle.Bottom);
            Point p_right_top = new Point(rectangle.Right, rectangle.Top);
            Point p_right_bottom = new Point(rectangle.Right, rectangle.Bottom);
            ColorBlend blend = new ColorBlend();
            blend.Colors = new Color[] { transparent, solid, transparent };
            blend.Positions = new float[] { 0, 0.5f, 1 };
            LinearGradientBrush b_left = new LinearGradientBrush(p_left_top, p_left_bottom, transparent, solid);
            LinearGradientBrush b_top = new LinearGradientBrush(p_left_top, p_right_top, transparent, solid);
            LinearGradientBrush b_right = new LinearGradientBrush(p_right_top, p_right_bottom, transparent, solid);
            LinearGradientBrush b_bottom = new LinearGradientBrush(p_left_bottom, p_right_bottom, transparent, solid);
            b_left.InterpolationColors = blend;
            b_top.InterpolationColors = blend;
            b_right.InterpolationColors = blend;
            b_bottom.InterpolationColors = blend;
            Pen pe_left = new Pen(b_left);
            Pen pe_top = new Pen(b_top);
            Pen pe_right = new Pen(b_right);
            Pen pe_bottom = new Pen(b_bottom);
            g.DrawLine(pe_left, p_left_top, p_left_bottom);
            g.DrawLine(pe_top, p_left_top, p_right_top);
            g.DrawLine(pe_right, p_right_top, p_right_bottom);
            g.DrawLine(pe_bottom, p_left_bottom, p_right_bottom);
            b_left.Dispose();
            b_top.Dispose();
            b_right.Dispose();
            b_bottom.Dispose();
            pe_left.Dispose();
            pe_top.Dispose();
            pe_right.Dispose();
            pe_bottom.Dispose();
        }

        public void LeaveDesktop(VDesktop desktop)
        {
        }

        public void Clear()
        {
        }

        public bool isClearNecessary()
        {
            return true;
        }
    }

    public class AeroPainter : WindowPainter
    {
        private List<IntPtr> windowThumbs = new List<IntPtr>();
        private IntPtr draggedThumb = IntPtr.Zero;
        private IntPtr destinationWindow;

        public AeroPainter(IntPtr destinationWindow)
        {
            this.destinationWindow = destinationWindow;
        }

        public void paintDesktop(Graphics g, bool isDraggingWindow, VDesktop desktop, int x, int y, int width, int height)
        {
            if (isDraggingWindow)
            {
                //only the dragged window requires
                //an update
                return;
            }
            List<VDesktopWindow> windows = desktop.GetOrderedWindows();
            foreach (VDesktopWindow window in windows)
            {
                if (!window.IsValid())
                {
                    //this window does not
                    //exist anymore despite
                    //of being listed as a window
                    //of this desktop
                    continue;
                }
                paintWindow(g, isDraggingWindow, x, y, width, height, window);
            }
        }

        private void paintWindow(Graphics g, bool isDraggingWindow, int x, int y, int width, int height, VDesktopWindow window)
        {
            paintWindow(g, isDraggingWindow, x, y, width, height, window, false, new Point());
        }

        public void paintWindow(Graphics g, bool isDraggingDifferentWindow, int x, int y, int width, int height, VDesktopWindow window, bool useDifferentPosition, Point differentPosition)
        {
            if (!window.IsValid())
            {
                //this window does not
                //exist anymore despite
                //of being listed as a window
                //of this desktop
                return;
            }
            Rectangle wRectangle = window.GetRectangle();
            Rectangle rectangle = DesktopsForm.Desktop2Preview(x, y, width, height, DesktopsForm.ClipDesktopWindow(wRectangle));
            if (rectangle.Bottom == 0 && rectangle.Top == 0 && rectangle.Width == 0 && rectangle.Height == 0)
            {
                //a clipped rectangle, don't paint it
                return;
            }
            if (useDifferentPosition)
            {
                //use differentPosition as the
                //upper left corner of the preview
                //of the window
                rectangle = new Rectangle(differentPosition.X, differentPosition.Y, rectangle.Width, rectangle.Height);
            }

            //register thumb, and move it to an appropriate position
            IntPtr windowThumb = IntPtr.Zero;
            int status = 0;
            if (!useDifferentPosition)
            {
                //definitely not dragging a window
                //hence there is no need to remember the last dragged thumb
                draggedThumb = IntPtr.Zero;
            }
            if (useDifferentPosition && draggedThumb != IntPtr.Zero)
            {
                //unregister dragged thumb before reinitialization
                try
                {
                    Dwmapi.DwmUnregisterThumbnail(draggedThumb);
                    windowThumbs.Remove(draggedThumb);
                }
                catch (Exception)
                {
                }
                draggedThumb = IntPtr.Zero;
            }
            Dwmapi.DwmRegisterThumbnail(destinationWindow, window.GetHandle(), out windowThumb);
            if (status == 0)
            {
                windowThumbs.Add(windowThumb);
                if (useDifferentPosition)
                {
                    //thumb is dragged, hence remember it as a dragged thumb
                    draggedThumb = windowThumb;
                }
                DWM_THUMBNAIL_PROPERTIES properties = new DWM_THUMBNAIL_PROPERTIES();
                properties.dwFlags = (int)(
                    DesktopWindowManager.DWM_TNP_VISIBLE
                    | DesktopWindowManager.DWM_TNP_RECTDESTINATION
                    | DesktopWindowManager.DWM_TNP_OPACITY
                    );
                properties.fVisible = true;
                properties.rcDestination = new Rect(rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Bottom);
                properties.opacity = 0xff;
                Dwmapi.DwmUpdateThumbnailProperties(windowThumb, ref properties);
            }
        }

        public void LeaveDesktop(VDesktop desktop)
        {
        }

        public void Clear()
        {
            foreach (IntPtr windowThumb in windowThumbs)
            {
                try
                {
                    Dwmapi.DwmUnregisterThumbnail(windowThumb);
                }
                catch (Exception)
                {
                }
            }
            windowThumbs.Clear();
        }

        public bool isClearNecessary()
        {
            return !Dwmapi.DwmIsCompositionEnabled();
        }
    }

    public class CachedAeroPainter: WindowPainter
    {
        private List<IntPtr> windowThumbs = new List<IntPtr>();
        private Dictionary<IntPtr, Bitmap> thumbnailCache = new Dictionary<IntPtr, Bitmap>();
        private IntPtr draggedThumb = IntPtr.Zero;
        private IntPtr destinationWindow;
        private int lastWidth = 240;
        private int lastHeight = 140;
        private double updateThumbnailRate = 0.3;

        public CachedAeroPainter(IntPtr destinationWindow, double updateThumbnailRate)
        {
            this.destinationWindow = destinationWindow;
            this.updateThumbnailRate = updateThumbnailRate;
        }

        public void SetUpdateThumbnailRate(double rate)
        {
            this.updateThumbnailRate = rate;
        }

        public double GetUpdateThumbnailRate()
        {
            return updateThumbnailRate;
        }

        public void LeaveDesktop(VDesktop desktop)
        {
            List<VDesktopWindow> windows = desktop.GetOrderedWindows();
            foreach (VDesktopWindow window in windows)
            {
                //reject update of thumbnail?
                Random random = new Random();
                double rNum = random.NextDouble();
                bool rejectUpdate = updateThumbnailRate < rNum;

                if (rejectUpdate && thumbnailCache.ContainsKey(window.GetHandle()))
                {
                    //thumbnail already in cache
                    //and for this time no update
                    //so go on to the next window
                    continue;
                }
                try
                {
                    Bitmap bmp = GetThumbnailCopy(window, 0, 0, lastWidth, lastHeight);
                    IntPtr windowHandle = window.GetHandle();
                    if (thumbnailCache.ContainsKey(windowHandle))
                    {
                        Bitmap oldBmp = thumbnailCache[windowHandle];
                        oldBmp.Dispose();
                    }
                    thumbnailCache[windowHandle] = bmp;
                }
                catch (Exception)
                {
                }
            }
        }

        public void paintDesktop(Graphics g, bool isDraggingWindow, VDesktop desktop, int x, int y, int width, int height)
        {
            lastWidth = width;
            lastHeight = height;
            List<VDesktopWindow> windows = desktop.GetOrderedWindows();
            foreach (VDesktopWindow window in windows)
            {
                if (!window.IsValid())
                {
                    //this window does not
                    //exist anymore despite
                    //of being listed as a window
                    //of this desktop
                    continue;
                }
                paintWindow(g, isDraggingWindow, x, y, width, height, window);
            }
        }

        private void paintWindow(Graphics g, bool isDraggingWindow, int x, int y, int width, int height, VDesktopWindow window)
        {
            paintWindow(g, isDraggingWindow, x, y, width, height, window, false, new Point());
        }

        public void paintWindow(Graphics g, bool isDraggingDifferentWindow, int x, int y, int width, int height, VDesktopWindow window, bool useDifferentPosition, Point differentPosition)
        {
            lastWidth = width;
            lastHeight = height;
            if (!window.IsValid())
            {
                //this window does not
                //exist anymore despite
                //of being listed as a window
                //of this desktop
                return;
            }
            //is the window not on the current desktop?
            VDesktopManager manager = VDesktopManager.Get();
            VDesktop currentDesktop = manager.GetCurrentDesktop();
            Bitmap cachedThumbnail = null;
            if (currentDesktop != null && !currentDesktop.GetWindows().ContainsKey(window.GetHandle()) && thumbnailCache.ContainsKey(window.GetHandle()))
            {
                //window not on current desktop
                //but found a cached thumbnail
                cachedThumbnail = thumbnailCache[window.GetHandle()];
                if (cachedThumbnail != null && (cachedThumbnail.Width <= 0 || cachedThumbnail.Height <= 0))
                {
                    //not a valid thumbnail
                    //don't use cached value
                    cachedThumbnail = null;
                }
            }

            //get rectangle
            Rectangle wRectangle = window.GetRectangle();
            Rectangle rectangle = DesktopsForm.Desktop2Preview(x, y, width, height, DesktopsForm.ClipDesktopWindow(wRectangle));
            if (rectangle.Bottom == 0 && rectangle.Top == 0 && rectangle.Width == 0 && rectangle.Height == 0)
            {
                //a clipped rectangle, don't paint it
                return;
            }
            if (useDifferentPosition)
            {
                //use differentPosition as the
                //upper left corner of the preview
                //of the window
                rectangle = new Rectangle(differentPosition.X, differentPosition.Y, rectangle.Width, rectangle.Height);
            }

            //register thumb, and move it to an appropriate position
            IntPtr windowThumb = IntPtr.Zero;
            int status = 0;
            if (!useDifferentPosition && !isDraggingDifferentWindow)
            {
                //definitely not dragging a window
                //hence there is no need to remember the last dragged thumb
                draggedThumb = IntPtr.Zero;
            }
            if (useDifferentPosition && draggedThumb != IntPtr.Zero)
            {
                //unregister dragged thumb before reinitialization
                try
                {
                    Dwmapi.DwmUnregisterThumbnail(draggedThumb);
                    windowThumbs.Remove(draggedThumb);
                }
                catch (Exception)
                {
                }
                draggedThumb = IntPtr.Zero;
            }
            if (cachedThumbnail == null)
            {
                if (isDraggingDifferentWindow)
                {
                    //another window is dragged
                    //hence no update is required
                    //for this dwm thumbnail
                    return;
                }
                //use live thumbnail
                Dwmapi.DwmRegisterThumbnail(destinationWindow, window.GetHandle(), out windowThumb);
                if (status == 0)
                {
                    windowThumbs.Add(windowThumb);
                    if (useDifferentPosition)
                    {
                        //thumb is dragged, hence remember it as a dragged thumb
                        draggedThumb = windowThumb;
                    }
                    DWM_THUMBNAIL_PROPERTIES properties = new DWM_THUMBNAIL_PROPERTIES();
                    properties.dwFlags = (int)(
                        DesktopWindowManager.DWM_TNP_VISIBLE
                        | DesktopWindowManager.DWM_TNP_RECTDESTINATION
                        | DesktopWindowManager.DWM_TNP_OPACITY
                        );
                    properties.fVisible = true;
                    properties.rcDestination = new Rect(rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Bottom);
                    properties.opacity = 0xff;
                    Dwmapi.DwmUpdateThumbnailProperties(windowThumb, ref properties);
                }
            }
            else
            {
                //use cached thumbnail
                try
                {
                    g.DrawImage(cachedThumbnail, rectangle);
                }
                catch (Exception)
                {
                }
            }
        }

        public void Clear()
        {
            try
            {
                //check thumbnailcache for cached thumbnails
                //without any window, and remove those cache items
                VDesktopManager manager = VDesktopManager.Get();
                List<VDesktop> vDesktops = manager.GetDesktops();
                List<IntPtr> hwndToRemove = new List<IntPtr>();
                foreach (IntPtr hwnd in thumbnailCache.Keys)
                {
                    bool removeHwnd = true;
                    foreach (VDesktop vDesktop in vDesktops)
                    {
                        if (vDesktop.GetWindows().ContainsKey(hwnd))
                        {
                            //found window in desktopmanager
                            //no need to remove thumbnail image
                            //from cache
                            removeHwnd = false;
                        }
                    }
                    if (removeHwnd)
                    {
                        hwndToRemove.Add(hwnd);
                    }
                }
                //remove thumbnails without corresponding window
                foreach (IntPtr hwnd in hwndToRemove)
                {
                    Bitmap bmp = thumbnailCache[hwnd];
                    bmp.Dispose();
                    thumbnailCache.Remove(hwnd);
                }
            }
            catch(Exception)
            {
            }

            //exec clear
            foreach (IntPtr windowThumb in windowThumbs)
            {
                try
                {
                    Dwmapi.DwmUnregisterThumbnail(windowThumb);
                }
                catch (Exception)
                {
                }
            }
            windowThumbs.Clear();
        }

        public bool isClearNecessary()
        {
            return !Dwmapi.DwmIsCompositionEnabled();
        }

        private Bitmap GetThumbnailCopy(VDesktopWindow window,int x, int y, int width, int height)
        {
            if (!window.IsValid())
            {
                throw new Exception();
            }
            Rectangle rect = window.GetRectangle();
            Rectangle aimRect = DesktopsForm.Desktop2Preview(x, y, width, height, DesktopsForm.ClipDesktopWindow(rect));
            IntPtr hdcSrc = User32.GetWindowDC(window.GetHandle());
            IntPtr hdcDest = Gdi32.CreateCompatibleDC(hdcSrc);
            IntPtr hBitmap = Gdi32.CreateCompatibleBitmap(hdcSrc, aimRect.Width, aimRect.Height);
            IntPtr hOld = Gdi32.SelectObject(hdcDest, hBitmap);
            Gdi32.SetStretchBltMode(hdcDest, StretchBltMode.STRETCH_HALFTONE);
            Gdi32.StretchBlt(hdcDest, 0, 0, aimRect.Width, aimRect.Height, hdcSrc, 0, 0, rect.Width, rect.Height, TernaryRasterOperations.SRCCOPY);
            Gdi32.SelectObject(hdcDest, hOld);
            Gdi32.DeleteDC(hdcDest);
            User32.ReleaseDC(window.GetHandle(), hdcSrc);

            Bitmap bmp = Bitmap.FromHbitmap(hBitmap);
            return bmp;
        }

    }



}
