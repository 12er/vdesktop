﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VDesktop
{
    public partial class SettingsForm : Form
    {
        private static SettingsForm currentSettingsForm = null;

        public static SettingsForm Get()
        {
            if (currentSettingsForm != null)
            {
                currentSettingsForm.Dispose();
                currentSettingsForm = null;
            }
            currentSettingsForm = new SettingsForm();
            return currentSettingsForm;
        }

        private SettingsForm()
        {
            InitializeComponent();
        }

        public enum HotKeyType
        {
            HKT_LEFT,
            HKT_RIGHT,
            HKT_MOVELEFT,
            HKT_MOVERIGHT,
            HKT_SHOW
        }

        private class HotKeysPanel : TableLayoutPanel
        {

            private class HotKeyPanel : FlowLayoutPanel
            {
                private Dictionary<String, int> name2Code;
                private Dictionary<int, String> code2Name;
                private Label ctrlLbl = new Label();
                private Label windowsLbl = new Label();
                private Label altLbl = new Label();
                private Label shiftLbl = new Label();
                private Label keyLbl = new Label();
                private CheckBox ctrlCB = new CheckBox();
                private CheckBox windowsCB = new CheckBox();
                private CheckBox altCB = new CheckBox();
                private CheckBox shiftCB = new CheckBox();
                private ComboBox keyCB = new ComboBox();
                private Button closeBtn = new Button();

                private Dictionary<String, int> GetName2Code()
                {
                    Dictionary<String, int> d = new Dictionary<String, int>() {
                    {"None", 0},
                    {"LButton", 1},
                    {"RButton", 2},
                    {"Cancel", 3},
                    {"MButton", 4},
                    {"XButton1", 5},
                    {"XButton2", 6},
                    {"Back", 8},
                    {"Tab", 9},
                    {"LineFeed", 10},
                    {"Clear", 12},
                    {"Enter", 13},
                    {"ShiftKey", 16},
                    {"ControlKey", 17},
                    {"Menu", 18},
                    {"Pause", 19},
                    {"CapsLock", 20},
                    {"KanaMode", 21},
                    {"JunjaMode", 23},
                    {"FinalMode", 24},
                    {"KanjiMode", 25},
                    {"Escape", 27},
                    {"IMEConvert", 28},
                    {"IMENonconvert", 29},
                    {"IMEAceept", 30},
                    {"IMEModeChange", 31},
                    {"Space", 32},
                    {"PageUp", 33},
                    {"PageDown", 34},
                    {"End", 35},
                    {"Home", 36},
                    {"Left", 37},
                    {"Up", 38},
                    {"Right", 39},
                    {"Down", 40},
                    {"Select", 41},
                    {"Print", 42},
                    {"Execute", 43},
                    {"Snapshot", 44},
                    {"Insert", 45},
                    {"Delete", 46},
                    {"Help", 47},
                    {"0", 48},
                    {"1", 49},
                    {"2", 50},
                    {"3", 51},
                    {"4", 52},
                    {"5", 53},
                    {"6", 54},
                    {"7", 55},
                    {"8", 56},
                    {"9", 57},
                    {"A", 65},
                    {"B", 66},
                    {"C", 67},
                    {"D", 68},
                    {"E", 69},
                    {"F", 70},
                    {"G", 71},
                    {"H", 72},
                    {"I", 73},
                    {"J", 74},
                    {"K", 75},
                    {"L", 76},
                    {"M", 77},
                    {"N", 78},
                    {"O", 79},
                    {"P", 80},
                    {"Q", 81},
                    {"R", 82},
                    {"S", 83},
                    {"T", 84},
                    {"U", 85},
                    {"V", 86},
                    {"W", 87},
                    {"X", 88},
                    {"Y", 89},
                    {"Z", 90},
                    {"LWin", 91},
                    {"RWin", 92},
                    {"Apps", 93},
                    {"Sleep", 95},
                    {"NumPad0", 96},
                    {"NumPad1", 97},
                    {"NumPad2", 98},
                    {"NumPad3", 99},
                    {"NumPad4", 100},
                    {"NumPad5", 101},
                    {"NumPad6", 102},
                    {"NumPad7", 103},
                    {"NumPad8", 104},
                    {"NumPad9", 105},
                    {"Multiply", 106},
                    {"Add", 107},
                    {"Separator", 108},
                    {"Subtract", 109},
                    {"Decimal", 110},
                    {"Divide", 111},
                    {"F1", 112},
                    {"F2", 113},
                    {"F3", 114},
                    {"F4", 115},
                    {"F5", 116},
                    {"F6", 117},
                    {"F7", 118},
                    {"F8", 119},
                    {"F9", 120},
                    {"F10", 121},
                    {"F11", 122},
                    {"F12", 123},
                    {"F13", 124},
                    {"F14", 125},
                    {"F15", 126},
                    {"F16", 127},
                    {"F17", 128},
                    {"F18", 129},
                    {"F19", 130},
                    {"F20", 131},
                    {"F21", 132},
                    {"F22", 133},
                    {"F23", 134},
                    {"F24", 135},
                    {"NumLock", 144},
                    {"Scroll", 145},
                    {"LShiftKey", 160},
                    {"RShiftKey", 161},
                    {"LControlKey", 162},
                    {"RControlKey", 163},
                    {"LMenu", 164},
                    {"RMenu", 165},
                    {"BrowserBack", 166},
                    {"BrowserForward", 167},
                    {"BrowserRefresh", 168},
                    {"BrowserStop", 169},
                    {"BrowserSearch", 170},
                    {"BrowserFavorites", 171},
                    {"BrowserHome", 172},
                    {"VolumeMute", 173},
                    {"VolumeDown", 174},
                    {"VolumeUp", 175},
                    {"MediaNextTrack", 176},
                    {"MediaPreviousTrack", 177},
                    {"MediaStop", 178},
                    {"MediaPlayPause", 179},
                    {"LaunchMail", 180},
                    {"SelectMedia", 181},
                    {"LaunchApplication1", 182},
                    {"LaunchApplication2", 183},
                    {"Oem1", 186},
                    {"Oemplus", 187},
                    {"Oemcomma", 188},
                    {"OemMinus", 189},
                    {"OemPeriod", 190},
                    {"Oem2", 191},
                    {"Oem3", 192},
                    {"Oem4", 219},
                    {"Oem5", 220},
                    {"Oem6", 221},
                    {"Oem7", 222},
                    {"Oem8", 223},
                    {"Oem102", 226},
                    {"ProcessKey", 229},
                    {"Packet", 231},
                    {"Attn", 246},
                    {"Crsel", 247},
                    {"Exsel", 248},
                    {"EraseEof", 249},
                    {"Play", 250},
                    {"Zoom", 251},
                    {"NoName", 252},
                    {"Pa1", 253},
                    {"OemClear", 254},
                    {"KeyCode", 65535},
                    {"Shift", 65536},
                    {"Control", 131072},
                    {"Alt", 262144},
                };

                    return d;
                }

                private Dictionary<int, String> GetCode2Name()
                {
                    Dictionary<String, int> dOrig = GetName2Code();
                    Dictionary<int, String> d = dOrig.ToDictionary(dO => dO.Value, dO => dO.Key);
                    return d;
                }

                private String[] GetKeyArray()
                {
                    String[] keyArray = 
                {
                "None",
                "LButton",
                "Cancel",
                "MButton",
                "XButton1",
                "XButton2",
                "Back",
                "Tab",
                "LineFeed",
                "Clear",
                "Enter",
                "ShiftKey",
                "ControlKey",
                "Menu",
                "Pause",
                "CapsLock",
                "KanaMode",
                "JunjaMode",
                "FinalMode",
                "KanjiMode",
                "Escape",
                "IMEConvert",
                "IMENonconvert",
                "IMEAceept",
                "IMEModeChange",
                "Space",
                "PageUp",
                "PageDown",
                "End",
                "Home",
                "Left",
                "Up",
                "Right",
                "Down",
                "Select",
                "Print",
                "Execute",
                "Snapshot",
                "Insert",
                "Delete",
                "Help",
                "0",
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "A",
                "B",
                "C",
                "D",
                "E",
                "F",
                "G",
                "H",
                "I",
                "J",
                "K",
                "L",
                "M",
                "N",
                "O",
                "P",
                "Q",
                "R",
                "S",
                "T",
                "U",
                "V",
                "W",
                "X",
                "Y",
                "Z",
                "LWin",
                "RWin",
                "Apps",
                "Sleep",
                "NumPad0",
                "NumPad1",
                "NumPad2",
                "NumPad3",
                "NumPad4",
                "NumPad5",
                "NumPad6",
                "NumPad7",
                "NumPad8",
                "NumPad9",
                "Multiply",
                "Add",
                "Separator",
                "Subtract",
                "Decimal",
                "Divide",
                "F1",
                "F2",
                "F3",
                "F4",
                "F5",
                "F6",
                "F7",
                "F8",
                "F9",
                "F10",
                "F11",
                "F12",
                "F13",
                "F14",
                "F15",
                "F16",
                "F17",
                "F18",
                "F19",
                "F20",
                "F21",
                "F22",
                "F23",
                "F24",
                "NumLock",
                "Scroll",
                "LShiftKey",
                "RShiftKey",
                "LControlKey",
                "RControlKey",
                "LMenu",
                "RMenu",
                "BrowserBack",
                "BrowserForward",
                "BrowserRefresh",
                "BrowserStop",
                "BrowserSearch",
                "BrowserFavorites",
                "BrowserHome",
                "VolumeMute",
                "VolumeDown",
                "VolumeUp",
                "MediaNextTrack",
                "MediaPreviousTrack",
                "MediaStop",
                "MediaPlayPause",
                "LaunchMail",
                "SelectMedia",
                "LaunchApplication1",
                "LaunchApplication2",
                "Oem1",
                "Oemplus",
                "Oemcomma",
                "OemMinus",
                "OemPeriod",
                "Oem2",
                "Oem3",
                "Oem4",
                "Oem5",
                "Oem6",
                "Oem7",
                "Oem8",
                "Oem102",
                "ProcessKey",
                "Packet",
                "Attn",
                "Crsel",
                "Exsel",
                "EraseEof",
                "Play",
                "Zoom",
                "NoName",
                "Pa1",
                "OemClear",
                "KeyCode",
                "Shift",
                "Control",
                "Alt",
                }
                    ;
                    return keyArray;
                }

                private bool datasourceInitialized = false;
                private int initKey = 0;
                private HotKeysPanel parent;

                public HotKeyPanel(HotKeysPanel parent, HotKey k)
                    : base()
                {
                    this.parent = parent;
                    AutoSize = true;
                    AutoSizeMode = AutoSizeMode.GrowAndShrink;

                    name2Code = GetName2Code();
                    code2Name = GetCode2Name();
                    ctrlLbl.Text = "Ctrl";
                    ctrlLbl.Size = new Size(23, 23);
                    windowsLbl.Text = "Windows";
                    windowsLbl.Size = new Size(55, 23);
                    altLbl.Text = "Alt";
                    altLbl.Size = new Size(23, 23);
                    shiftLbl.Text = "Shift";
                    shiftLbl.Size = new Size(30, 23);
                    keyLbl.Text = "Key";
                    keyLbl.Size = new Size(25, 23);
                    ctrlCB.Checked = (k.keyModifier & KeyModifier.KM_CTRL) != 0;
                    ctrlCB.CheckedChanged += new EventHandler(EditHotKey);
                    ctrlCB.Size = new Size(30, 23);
                    windowsCB.Checked = (k.keyModifier & KeyModifier.KM_WINDOWS) != 0;
                    windowsCB.Size = new Size(30, 23);
                    windowsCB.CheckedChanged += new EventHandler(EditHotKey);
                    altCB.Checked = (k.keyModifier & KeyModifier.KM_ALT) != 0;
                    altCB.Size = new Size(30, 23);
                    altCB.CheckedChanged += new EventHandler(EditHotKey);
                    shiftCB.Checked = (k.keyModifier & KeyModifier.KM_SHIFT) != 0;
                    shiftCB.Size = new Size(30, 23);
                    shiftCB.CheckedChanged += new EventHandler(EditHotKey);
                    keyCB.Items.AddRange(GetKeyArray());
                    keyCB.Size = new Size(75, 23);
                    keyCB.SelectedIndex = 0;

                    if (code2Name.ContainsKey((int)k.key))
                    {
                        String kString = code2Name[(int)k.key];
                        for (int i = 0; i < keyCB.Items.Count; i++)
                        {
                            if (((String)keyCB.Items[i]).Equals(kString))
                            {
                                keyCB.SelectedIndex = i;
                            }
                        }
                    }

                    keyCB.SelectionChangeCommitted += new EventHandler(EditHotKey);

                    closeBtn.Image = Image.FromFile("./close_unfocused.png");
                    closeBtn.BackgroundImageLayout = ImageLayout.Stretch;
                    closeBtn.Size = new Size(24, 24);
                    closeBtn.Click += new EventHandler(RemoveHotKey);

                    Controls.Add(ctrlLbl);
                    Controls.Add(ctrlCB);
                    Controls.Add(windowsLbl);
                    Controls.Add(windowsCB);
                    Controls.Add(altLbl);
                    Controls.Add(altCB);
                    Controls.Add(shiftLbl);
                    Controls.Add(shiftCB);
                    Controls.Add(keyLbl);
                    Controls.Add(keyCB);
                    Controls.Add(closeBtn);
                }

                public HotKey GetHotKey()
                {
                    HotKey hk = new HotKey();
                    hk.keyModifier = (KeyModifier)0;
                    if (ctrlCB.Checked)
                    {
                        hk.keyModifier = hk.keyModifier | KeyModifier.KM_CTRL;
                    }
                    if (windowsCB.Checked)
                    {
                        hk.keyModifier = hk.keyModifier | KeyModifier.KM_WINDOWS;
                    }
                    if (altCB.Checked)
                    {
                        hk.keyModifier = hk.keyModifier | KeyModifier.KM_ALT;
                    }
                    if (shiftCB.Checked)
                    {
                        hk.keyModifier = hk.keyModifier | KeyModifier.KM_SHIFT;
                    }
                    hk.key = 0;
                    try
                    {
                        String keyName = (String)(keyCB.Items[keyCB.SelectedIndex]);
                        hk.key = (Keys)name2Code[keyName];
                    }
                    catch (Exception)
                    {
                    }
                    return hk;
                }

                private void EditHotKey(object o, EventArgs e)
                {
                    parent.EditHotKey(this);
                }

                private void RemoveHotKey(object o, EventArgs e)
                {
                    parent.RemoveHotKey(this);
                }

            }

            private HotKeyType hotKeyType;

            public HotKeysPanel(HotKeyType hotKeyType, List<HotKey> k)
                : base()
            {
                this.hotKeyType = hotKeyType;
                AutoSize = true;

                Button addButton = new Button();
                addButton.Image = Image.FromFile("./new.png");
                addButton.BackgroundImageLayout = ImageLayout.Stretch;
                addButton.Size = new Size(32, 32);
                addButton.Click += new EventHandler(AddHotKey);
                Controls.Add(addButton, 0, 0);
                int i = 0;
                foreach (HotKey key in k)
                {
                    HotKeyPanel hkPanel = new HotKeyPanel(this, key);
                    Controls.Add(hkPanel, 1, i);
                    i++;
                }
            }

            private List<HotKey> GetHotKeyList()
            {
                switch (hotKeyType)
                {
                    case HotKeyType.HKT_LEFT:
                        return Settings.Get().GetKeyLeft();
                    case HotKeyType.HKT_RIGHT:
                        return Settings.Get().GetKeyRight();
                    case HotKeyType.HKT_MOVELEFT:
                        return Settings.Get().GetKeyMoveLeft();
                    case HotKeyType.HKT_MOVERIGHT:
                        return Settings.Get().GetKeyMoveRight();
                    case HotKeyType.HKT_SHOW:
                        return Settings.Get().GetKeyShow();
                    default:
                        return Settings.Get().GetKeyShow();
                }
            }

            private void SetHotKeyList(List<HotKey> l)
            {
                switch (hotKeyType)
                {
                    case HotKeyType.HKT_LEFT:
                        Settings.Get().SetKeyLeft(l);
                        break;
                    case HotKeyType.HKT_RIGHT:
                        Settings.Get().SetKeyRight(l);
                        break;
                    case HotKeyType.HKT_MOVELEFT:
                        Settings.Get().SetKeyMoveLeft(l);
                        break;
                    case HotKeyType.HKT_MOVERIGHT:
                        Settings.Get().SetKeyMoveRight(l);
                        break;
                    case HotKeyType.HKT_SHOW:
                        Settings.Get().SetKeyShow(l);
                        break;
                    default:
                        Settings.Get().SetKeyShow(l);
                        break;
                }
            }

            public void AddHotKey(object o, EventArgs e)
            {
                try
                {
                    int i = Controls.Count - 1;
                    HotKey hkey = new HotKey();
                    HotKeyPanel hkPanel = new HotKeyPanel(this, hkey);
                    Controls.Add(hkPanel, 1, i);

                    List<HotKey> l = GetHotKeyList();
                    l.Add(hkey);
                    SetHotKeyList(l);

                    DesktopsForm.Get().UpdateHotKeys();
                }
                catch (Exception)
                {
                }
            }

            private void EditHotKey(HotKeyPanel p)
            {
                try
                {
                    int i = Controls.GetChildIndex(p) - 1;

                    List<HotKey> l = GetHotKeyList();
                    l[i] = p.GetHotKey();
                    SetHotKeyList(l);

                    DesktopsForm.Get().UpdateHotKeys();
                }
                catch (Exception)
                {
                }
            }

            private void RemoveHotKey(HotKeyPanel p)
            {
                try
                {
                    int i = Controls.GetChildIndex(p) - 1;

                    //remove hotkey panel
                    Controls.Remove(p);

                    //rearrange remaining hotkey panels
                    int ccount = Controls.Count;
                    List<Control> controlList = new List<Control>();
                    for (int j = 1; j < ccount; j++)
                    {
                        Control c = Controls[j];
                        controlList.Add(c);
                    }
                    foreach (Control c in controlList)
                    {
                        Controls.Remove(c);
                    }
                    for (int j = 1; j < ccount; j++)
                    {
                        Control c = controlList[j - 1];
                        Controls.Add(c, 1, j - 1);
                    }

                    List<HotKey> l = GetHotKeyList();
                    l.RemoveAt(i);
                    SetHotKeyList(l);

                    DesktopsForm.Get().UpdateHotKeys();
                }
                catch (Exception)
                {
                }
            }

        }

        private bool isSettingPreviewHeight = false;

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            FormClosed += new FormClosedEventHandler(SettingsFormClosed);

            //get controls
            NumericUpDown desktopCountUD = (NumericUpDown)this.Controls.Find("DesktopCountUD", true)[0];
            CheckBox startingWithWindowsCB = (CheckBox)this.Controls.Find("startingWithWindowsCB", true)[0];
            CheckBox aeroCB = (CheckBox)this.Controls.Find("aeroCB", true)[0];
            ComboBox emphasizeDesktopCB = (ComboBox)this.Controls.Find("EmphasizeDesktopCB", true)[0];
            ComboBox thumbnailViewCB = (ComboBox)this.Controls.Find("ThumbnailViewCB", true)[0];
            CheckBox addCloseCB = (CheckBox)this.Controls.Find("addCloseCB", true)[0];
            CheckBox unhideOnExitCB = (CheckBox)this.Controls.Find("unhideOnExitCB", true)[0];
            CheckBox bordersCB = (CheckBox)this.Controls.Find("bordersCB", true)[0];
            CheckBox tooltipCB = (CheckBox)this.Controls.Find("tooltipCB", true)[0];
            TrackBar updateRateTB = (TrackBar)this.Controls.Find("UpdateRateTB", true)[0];
            Label updateRateLabel = (Label)this.Controls.Find("UpdateRateLabel", true)[0];
            Label updateRateLabel2 = (Label)this.Controls.Find("updateRateLabel2", true)[0];
            TabPage hotKeysPage = (TabPage)this.Controls.Find("HotKeysPage", true)[0];
            PictureBox aboutImg = (PictureBox)this.Controls.Find("aboutImg", true)[0];
            
            //show general settings
            desktopCountUD.Value = Settings.Get().GetDesktopCount();
            startingWithWindowsCB.CheckedChanged -= startingWithWindowsCB_CheckedChanged;
            startingWithWindowsCB.Checked = Settings.Get().GetStartingWithWindows();
            startingWithWindowsCB.CheckedChanged += startingWithWindowsCB_CheckedChanged;
            aeroCB.Checked = Settings.Get().GetUseAero();
            addCloseCB.Checked = Settings.Get().GetUseAddCloseButtons();
            unhideOnExitCB.Checked = Settings.Get().GetUseUnhideOnExit();
            bordersCB.Checked = Settings.Get().GetUseBorders();
            tooltipCB.Checked = Settings.Get().GetUseToolTip();
            String thumbnailViewName;
            switch (Settings.Get().GetThumbnailView())
            {
                case ThumbnailView.CachedAeroWindows:
                    thumbnailViewName = Settings.CachedAeroWindows;
                    break;
                case ThumbnailView.AeroWindows:
                    thumbnailViewName = Settings.AeroWindows;
                    break;
                case ThumbnailView.BasicWindows:
                    thumbnailViewName = Settings.BasicWindows;
                    break;
                default:
                    thumbnailViewName = Settings.CachedAeroWindows;
                    break;
            }
            for(int i=0 ; i<thumbnailViewCB.Items.Count ; i++)
            {
                if (thumbnailViewCB.Items[i].ToString() == thumbnailViewName)
                {
                    thumbnailViewCB.SelectedIndex = i;
                }
            }
            if (Settings.Get().GetThumbnailView() == ThumbnailView.CachedAeroWindows)
            {
                updateRateLabel.Text = "" + Settings.Get().GetUpdateThumbnailRate();
                int rate = Math.Min(Math.Max((int)(Settings.Get().GetUpdateThumbnailRate() * 100), 0), 100);
                updateRateTB.Value = rate;
            }
            else
            {
                updateRateLabel2.Visible = false;
                updateRateLabel.Visible = false;
                updateRateTB.Visible = false;
            }
            String emphasizeDesktopName;
            switch (Settings.Get().GetEmphasizeDesktop())
            {
                case EmphasizeDesktop.BorderedEmphasizer:
                    emphasizeDesktopName = Settings.BorderedEmphasizer;
                    break;
                case EmphasizeDesktop.SmoothEmphasizer:
                    emphasizeDesktopName = Settings.SmoothEmphasizer;
                    break;
                case EmphasizeDesktop.NoEmphasizer:
                    emphasizeDesktopName = Settings.NoEmphasizer;
                    break;
                default:
                    emphasizeDesktopName = Settings.BorderedEmphasizer;
                    break;
            }
            for (int i = 0; i < emphasizeDesktopCB.Items.Count; i++)
            {
                if (emphasizeDesktopCB.Items[i].ToString() == emphasizeDesktopName)
                {
                    emphasizeDesktopCB.SelectedIndex = i;
                }
            }

            //show hotkey settings
            int locCounter = 0;
            TableLayoutPanel hotKeysPanel = new TableLayoutPanel();
            hotKeysPanel.AutoSize = true;
            hotKeysPanel.Location = new Point(21, 26);
            hotKeysPage.Controls.Add(hotKeysPanel);

            //left desktop
            Label leftDesktopLabel = new Label();
            leftDesktopLabel.Text = "Left Desktop";
            leftDesktopLabel.Size = new Size(75,23);
            hotKeysPanel.Controls.Add(leftDesktopLabel, 0, locCounter);
            hotKeysPanel.Controls.Add(new HotKeysPanel(HotKeyType.HKT_LEFT, Settings.Get().GetKeyLeft()), 1, locCounter);

            SeparatingLine l1 = new SeparatingLine();
            l1.Dock = DockStyle.Top;
            hotKeysPanel.Controls.Add(l1, 0, ++locCounter);
            hotKeysPanel.SetColumnSpan(l1, 2);
            hotKeysPanel.SetRowSpan(l1, 1);

            //right desktop
            Label rightDesktopLabel = new Label();
            rightDesktopLabel.Text = "Right Desktop";
            rightDesktopLabel.Size = new Size(75, 23);
            hotKeysPanel.Controls.Add(rightDesktopLabel, 0, ++locCounter);
            hotKeysPanel.Controls.Add(new HotKeysPanel(HotKeyType.HKT_RIGHT, Settings.Get().GetKeyRight()), 1, locCounter);

            SeparatingLine l2 = new SeparatingLine();
            l2.Dock = DockStyle.Top;
            hotKeysPanel.Controls.Add(l2, 0, ++locCounter);
            hotKeysPanel.SetColumnSpan(l2, 2);
            hotKeysPanel.SetRowSpan(l2, 1);

            //move window left
            Label leftWindowLabel = new Label();
            leftWindowLabel.Text = "Move window left";
            leftWindowLabel.Size = new Size(100, 23);
            hotKeysPanel.Controls.Add(leftWindowLabel, 0, ++locCounter);
            hotKeysPanel.Controls.Add(new HotKeysPanel(HotKeyType.HKT_MOVELEFT, Settings.Get().GetKeyMoveLeft()), 1, locCounter);

            SeparatingLine l3 = new SeparatingLine();
            l3.Dock = DockStyle.Top;
            hotKeysPanel.Controls.Add(l3, 0, ++locCounter);
            hotKeysPanel.SetColumnSpan(l3, 2);
            hotKeysPanel.SetRowSpan(l3, 1); 

            //move window right
            Label rightWindowLabel = new Label();
            rightWindowLabel.Text = "Move window right";
            rightWindowLabel.Size = new Size(100, 23);
            hotKeysPanel.Controls.Add(rightWindowLabel, 0, ++locCounter);
            hotKeysPanel.Controls.Add(new HotKeysPanel(HotKeyType.HKT_MOVERIGHT, Settings.Get().GetKeyMoveRight()), 1, locCounter);

            SeparatingLine l4 = new SeparatingLine();
            l4.Dock = DockStyle.Top;
            hotKeysPanel.Controls.Add(l4, 0, ++locCounter);
            hotKeysPanel.SetColumnSpan(l4, 2);
            hotKeysPanel.SetRowSpan(l4, 1);

            //show preview
            Label showLabel = new Label();
            showLabel.Text = "Show Desktops";
            showLabel.Size = new Size(75, 23);
            hotKeysPanel.Controls.Add(showLabel, 0, ++locCounter);
            hotKeysPanel.Controls.Add(new HotKeysPanel(HotKeyType.HKT_SHOW, Settings.Get().GetKeyShow()), 1, locCounter);

            //about tab
            aboutImg.Image = new Bitmap("./VDesktopAbout.png");
            aboutImg.SizeMode = PictureBoxSizeMode.AutoSize;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void DesktopCountUD_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown desktopCountUD = (NumericUpDown)this.Controls.Find("DesktopCountUD", true)[0];
            int count = (int)desktopCountUD.Value;
            VDesktopManager vdm = VDesktopManager.Get();
            List<VDesktop> desktops = vdm.GetDesktops();
            VDesktopForm vDesktopForm = VDesktopForm.Get();
            DesktopsForm overview = DesktopsForm.Get();
            int oldCount = desktops.Count;
            if (count <= 0)
            {
            }
            else if (count > oldCount)
            {
                vDesktopForm.Invoke(new Action(() =>
                    {
                        while(desktops.Count < count)
                        {
                            vDesktopForm.AddDesktop();
                        }
                    }
                    ));
                overview.Invoke(new Action(() =>
                {
                    overview.Update();
                    overview.Invalidate();
                    overview.Left -= overview.GetPreviewWidth();
                }));
            }
            else if (count < oldCount)
            {
                vDesktopForm.Invoke(new Action(() =>
                {
                    while (desktops.Count > count)
                    {
                        vDesktopForm.RemoveDesktop();
                    }
                }
                   ));
                overview.Invoke(new Action(() =>
                {
                    overview.Left += overview.GetPreviewWidth();
                    overview.Update();
                    overview.Invalidate();
                }));
            }

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void ThumbnailViewCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox thumbnailViewCB = (ComboBox)this.Controls.Find("ThumbnailViewCB", true)[0];
            TrackBar updateRateTB = (TrackBar)this.Controls.Find("UpdateRateTB", true)[0];
            Label updateRateLabel = (Label)this.Controls.Find("UpdateRateLabel", true)[0];
            Label updateRateLabel2 = (Label)this.Controls.Find("updateRateLabel2", true)[0];
            
            String thumbnailViewString = thumbnailViewCB.Items[thumbnailViewCB.SelectedIndex].ToString();
            ThumbnailView thumbnailView = ThumbnailView.CachedAeroWindows;
            if (thumbnailViewString.Equals(Settings.CachedAeroWindows))
            {
                thumbnailView = ThumbnailView.CachedAeroWindows;
            }
            else if (thumbnailViewString.Equals(Settings.AeroWindows))
            {
                thumbnailView = ThumbnailView.AeroWindows;
            }
            else if (thumbnailViewString.Equals(Settings.BasicWindows))
            {
                thumbnailView = ThumbnailView.BasicWindows;
            }
            if (thumbnailView == ThumbnailView.CachedAeroWindows)
            {
                updateRateLabel.Text = "" + Settings.Get().GetUpdateThumbnailRate();
                int rate = Math.Min(Math.Max((int)(Settings.Get().GetUpdateThumbnailRate() * 100), 0), 100);
                updateRateTB.Value = rate;
                updateRateTB.Visible = true;
                updateRateLabel.Visible = true;
                updateRateLabel2.Visible = true;
            }
            else
            {
                updateRateTB.Visible = false;
                updateRateLabel.Visible = false;
                updateRateLabel2.Visible = false;
            }
            DesktopsForm desktopsForm = DesktopsForm.Get();
            desktopsForm.Invoke(new Action(
                () =>
                {
                    desktopsForm.SetThumbnailView(thumbnailView);
                }
                ));
            Settings.Get().SetThumbnailView(thumbnailView);
        }

        private void UpdateRateTB_Scroll(object sender, EventArgs e)
        {
            TrackBar updateRateTB = (TrackBar)this.Controls.Find("UpdateRateTB", true)[0];
            Label updateRateLabel = (Label)this.Controls.Find("UpdateRateLabel", true)[0];
            double rate = 0.01 * (double)updateRateTB.Value;
            updateRateLabel.Text = "" + rate;
            DesktopsForm desktopsForm = DesktopsForm.Get();
            desktopsForm.Invoke(new Action(
                () =>
                {
                    desktopsForm.SetUpdateThumbnailRate(rate);
                }
                ));
            Settings.Get().SetUpdateThumbnailRate(rate);
        }

        private void showAllWindowsBtn_Click(object sender, EventArgs e)
        {
            VDesktopManager vdm = VDesktopManager.Get();
            VDesktop desktop = vdm.GetCurrentDesktop();

            DialogResult result = MessageBox.Show("Do you really want to proceed? System application windows may be shown. Click OK to proceed.", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
            if (result == DialogResult.OK)
            {
                desktop.InsertHiddenWindows();
            }
        }

        private void hideAllWindowsBtn_Click(object sender, EventArgs e)
        {
            VDesktopManager vdm = VDesktopManager.Get();
            VDesktop desktop = vdm.GetCurrentDesktop();

            DialogResult result = MessageBox.Show("Do you really want to proceed? The hidden windows may be only accessible by using the \"Show hidden windows\" button. Click OK to proceed.", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
            if (result == DialogResult.OK)
            {
                desktop.HideAndRemoveWindows();
            }
        }

        private void EmphasizeDesktopCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox emphasizeDesktopCB = (ComboBox)this.Controls.Find("EmphasizeDesktopCB", true)[0];

            String emphasizeDesktopString = emphasizeDesktopCB.Items[emphasizeDesktopCB.SelectedIndex].ToString();
            EmphasizeDesktop emphasizeDesktop = EmphasizeDesktop.BorderedEmphasizer;
            if (emphasizeDesktopString.Equals(Settings.BorderedEmphasizer))
            {
                emphasizeDesktop = EmphasizeDesktop.BorderedEmphasizer;
            }
            else if (emphasizeDesktopString.Equals(Settings.SmoothEmphasizer))
            {
                emphasizeDesktop = EmphasizeDesktop.SmoothEmphasizer;
            }
            else if (emphasizeDesktopString.Equals(Settings.NoEmphasizer))
            {
                emphasizeDesktop = EmphasizeDesktop.NoEmphasizer;
            }
            DesktopsForm desktopsForm = DesktopsForm.Get();
            desktopsForm.Invoke(new Action(
                () =>
                {
                    desktopsForm.SetEmphasizeDesktop(emphasizeDesktop);
                }
                ));
            Settings.Get().SetEmphasizeDesktop(emphasizeDesktop);
        }

        private void aeroCB_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox aeroCB = (CheckBox)this.Controls.Find("aeroCB", true)[0];

            DesktopsForm desktopsForm = DesktopsForm.Get();
            desktopsForm.Invoke(new Action(
                () =>
                {
                    desktopsForm.SetUseAero(aeroCB.Checked);
                }
                ));
            Settings.Get().SetUseAero(aeroCB.Checked);
        }

        private void addCloseCB_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox addCloseCB = (CheckBox)this.Controls.Find("addCloseCB", true)[0];

            DesktopsForm desktopsForm = DesktopsForm.Get();
            desktopsForm.Invoke(new Action(
                () =>
                {
                    desktopsForm.SetUseAddCloseButtons(addCloseCB.Checked);
                }
                ));
            Settings.Get().SetUseAddCloseButtons(addCloseCB.Checked);
        }

        private void unhideOnExitCB_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox unhideOnExitCB = (CheckBox)this.Controls.Find("unhideOnExitCB", true)[0];

            DesktopsForm desktopsForm = DesktopsForm.Get();
            desktopsForm.Invoke(new Action(
                () =>
                {
                    // :) :liebdreinschau:
                }
                ));
            Settings.Get().SetUseUnhideOnExit(unhideOnExitCB.Checked);
        }

        private void bordersCB_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox bordersCB = (CheckBox)this.Controls.Find("bordersCB", true)[0];

            DesktopsForm desktopsForm = DesktopsForm.Get();
            desktopsForm.Invoke(new Action(
                () =>
                {
                    desktopsForm.SetUseBorders(bordersCB.Checked);
                }
                ));
            Settings.Get().SetUseBorders(bordersCB.Checked);
        }

        private void tooltipCB_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox tooltipCB = (CheckBox)this.Controls.Find("tooltipCB", true)[0];

            DesktopsForm desktopsForm = DesktopsForm.Get();
            desktopsForm.Invoke(new Action(
                () =>
                {
                    desktopsForm.SetUseTooltip(tooltipCB.Checked);
                }
                ));
            Settings.Get().SetUseTooltip(tooltipCB.Checked);
        }

        private void previewHeightBtn_Click(object sender, EventArgs e)
        {
            Button previewHeightBtn = (Button)this.Controls.Find("previewHeightBtn", true)[0];
            if (!isSettingPreviewHeight)
            {
                DesktopsForm form = DesktopsForm.Get();
                form.Invoke(new Action(() =>
                    {
                        form.SetEditPreviewHeight(true);
                        form.Present();
                    }));
                previewHeightBtn.Text = "Save new Size";
            }
            else
            {
                SavePreviewHeight();
                previewHeightBtn.Text = "Change Size of Preview Window";
            }
            isSettingPreviewHeight = !isSettingPreviewHeight;
        }

        public void SavePreviewHeight()
        {
            DesktopsForm form = DesktopsForm.Get();
            form.Invoke(new Action(() =>
            {
                form.SetEditPreviewHeight(false);
                form.StopPresenting();
                Settings.Get().SetPreviewHeight(form.GetPreviewHeight());
            }));
        }

        new public void Dispose()
        {
            if (isSettingPreviewHeight)
            {
                SavePreviewHeight();
                isSettingPreviewHeight = false;
            }
            base.Dispose();
        }

        private void SettingsFormClosed(object o, FormClosedEventArgs args)
        {
            Dispose();
        }

        private void startingWithWindowsCB_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox startingWithWindowsCB = (CheckBox)this.Controls.Find("startingWithWindowsCB", true)[0];

            Settings.Get().SetStartingWithWindows(startingWithWindowsCB.Checked);
        }

    }
}
