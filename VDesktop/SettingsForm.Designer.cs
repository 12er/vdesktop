﻿namespace VDesktop
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.AdvancedTb = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.aboutImg = new System.Windows.Forms.PictureBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.startingWithWindowsCB = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.addCloseCB = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DesktopCountUD = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.previewHeightBtn = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.UpdateRateTB = new System.Windows.Forms.TrackBar();
            this.UpdateRateLabel = new System.Windows.Forms.Label();
            this.updateRateLabel2 = new System.Windows.Forms.Label();
            this.ThumbnailViewCB = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tooltipCB = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.bordersCB = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.aeroCB = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.EmphasizeDesktopCB = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.HotKeysPage = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.hideAllWindowsBtn = new System.Windows.Forms.Button();
            this.showAllWindowsBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.unhideOnExitCB = new System.Windows.Forms.CheckBox();
            this.AdvancedTb.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aboutImg)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DesktopCountUD)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpdateRateTB)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // AdvancedTb
            // 
            this.AdvancedTb.Controls.Add(this.tabPage3);
            this.AdvancedTb.Controls.Add(this.tabPage1);
            this.AdvancedTb.Controls.Add(this.tabPage4);
            this.AdvancedTb.Controls.Add(this.HotKeysPage);
            this.AdvancedTb.Controls.Add(this.tabPage2);
            this.AdvancedTb.Location = new System.Drawing.Point(12, 12);
            this.AdvancedTb.Name = "AdvancedTb";
            this.AdvancedTb.SelectedIndex = 0;
            this.AdvancedTb.Size = new System.Drawing.Size(657, 288);
            this.AdvancedTb.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.aboutImg);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(649, 262);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "About";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // aboutImg
            // 
            this.aboutImg.Location = new System.Drawing.Point(6, 6);
            this.aboutImg.Name = "aboutImg";
            this.aboutImg.Size = new System.Drawing.Size(134, 140);
            this.aboutImg.TabIndex = 0;
            this.aboutImg.TabStop = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.unhideOnExitCB);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.startingWithWindowsCB);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.addCloseCB);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.DesktopCountUD);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(649, 262);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // startingWithWindowsCB
            // 
            this.startingWithWindowsCB.AutoSize = true;
            this.startingWithWindowsCB.Location = new System.Drawing.Point(198, 52);
            this.startingWithWindowsCB.Name = "startingWithWindowsCB";
            this.startingWithWindowsCB.Size = new System.Drawing.Size(15, 14);
            this.startingWithWindowsCB.TabIndex = 16;
            this.startingWithWindowsCB.UseVisualStyleBackColor = true;
            this.startingWithWindowsCB.CheckedChanged += new System.EventHandler(this.startingWithWindowsCB_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(21, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "start with Windows";
            // 
            // addCloseCB
            // 
            this.addCloseCB.AutoSize = true;
            this.addCloseCB.Location = new System.Drawing.Point(198, 72);
            this.addCloseCB.Name = "addCloseCB";
            this.addCloseCB.Size = new System.Drawing.Size(15, 14);
            this.addCloseCB.TabIndex = 14;
            this.addCloseCB.UseVisualStyleBackColor = true;
            this.addCloseCB.CheckedChanged += new System.EventHandler(this.addCloseCB_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(165, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "use Close/Add-button on preview";
            // 
            // DesktopCountUD
            // 
            this.DesktopCountUD.Location = new System.Drawing.Point(198, 26);
            this.DesktopCountUD.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.DesktopCountUD.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.DesktopCountUD.Name = "DesktopCountUD";
            this.DesktopCountUD.Size = new System.Drawing.Size(120, 20);
            this.DesktopCountUD.TabIndex = 2;
            this.DesktopCountUD.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.DesktopCountUD.ValueChanged += new System.EventHandler(this.DesktopCountUD_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Number of Virtual Desktops";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.previewHeightBtn);
            this.tabPage4.Controls.Add(this.label9);
            this.tabPage4.Controls.Add(this.UpdateRateTB);
            this.tabPage4.Controls.Add(this.UpdateRateLabel);
            this.tabPage4.Controls.Add(this.updateRateLabel2);
            this.tabPage4.Controls.Add(this.ThumbnailViewCB);
            this.tabPage4.Controls.Add(this.label1);
            this.tabPage4.Controls.Add(this.tooltipCB);
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Controls.Add(this.bordersCB);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.aeroCB);
            this.tabPage4.Controls.Add(this.label5);
            this.tabPage4.Controls.Add(this.EmphasizeDesktopCB);
            this.tabPage4.Controls.Add(this.label4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(649, 262);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "Appearance";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // previewHeightBtn
            // 
            this.previewHeightBtn.Location = new System.Drawing.Point(198, 53);
            this.previewHeightBtn.Name = "previewHeightBtn";
            this.previewHeightBtn.Size = new System.Drawing.Size(191, 23);
            this.previewHeightBtn.TabIndex = 18;
            this.previewHeightBtn.Text = "Change Size of Preview Window";
            this.previewHeightBtn.UseVisualStyleBackColor = true;
            this.previewHeightBtn.Click += new System.EventHandler(this.previewHeightBtn_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 53);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Preview Size";
            // 
            // UpdateRateTB
            // 
            this.UpdateRateTB.BackColor = System.Drawing.SystemColors.Window;
            this.UpdateRateTB.LargeChange = 20;
            this.UpdateRateTB.Location = new System.Drawing.Point(223, 169);
            this.UpdateRateTB.Maximum = 100;
            this.UpdateRateTB.Name = "UpdateRateTB";
            this.UpdateRateTB.Size = new System.Drawing.Size(182, 45);
            this.UpdateRateTB.TabIndex = 5;
            this.UpdateRateTB.TickStyle = System.Windows.Forms.TickStyle.None;
            this.UpdateRateTB.Scroll += new System.EventHandler(this.UpdateRateTB_Scroll);
            // 
            // UpdateRateLabel
            // 
            this.UpdateRateLabel.AutoSize = true;
            this.UpdateRateLabel.Location = new System.Drawing.Point(195, 169);
            this.UpdateRateLabel.Name = "UpdateRateLabel";
            this.UpdateRateLabel.Size = new System.Drawing.Size(22, 13);
            this.UpdateRateLabel.TabIndex = 6;
            this.UpdateRateLabel.Text = "0.3";
            // 
            // updateRateLabel2
            // 
            this.updateRateLabel2.AutoSize = true;
            this.updateRateLabel2.Location = new System.Drawing.Point(21, 169);
            this.updateRateLabel2.Name = "updateRateLabel2";
            this.updateRateLabel2.Size = new System.Drawing.Size(155, 13);
            this.updateRateLabel2.TabIndex = 4;
            this.updateRateLabel2.Text = "Update rate of cached Thumbs";
            // 
            // ThumbnailViewCB
            // 
            this.ThumbnailViewCB.FormattingEnabled = true;
            this.ThumbnailViewCB.Items.AddRange(new object[] {
            "Cached Aero Windows",
            "Aero Windows",
            "Basic Windows"});
            this.ThumbnailViewCB.Location = new System.Drawing.Point(198, 142);
            this.ThumbnailViewCB.Name = "ThumbnailViewCB";
            this.ThumbnailViewCB.Size = new System.Drawing.Size(207, 21);
            this.ThumbnailViewCB.TabIndex = 3;
            this.ThumbnailViewCB.SelectedIndexChanged += new System.EventHandler(this.ThumbnailViewCB_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 142);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Appearance of preview Windows";
            // 
            // tooltipCB
            // 
            this.tooltipCB.AutoSize = true;
            this.tooltipCB.Location = new System.Drawing.Point(198, 122);
            this.tooltipCB.Name = "tooltipCB";
            this.tooltipCB.Size = new System.Drawing.Size(15, 14);
            this.tooltipCB.TabIndex = 16;
            this.tooltipCB.UseVisualStyleBackColor = true;
            this.tooltipCB.CheckedChanged += new System.EventHandler(this.tooltipCB_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 122);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(152, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "show Tooltip on hovered Items";
            // 
            // bordersCB
            // 
            this.bordersCB.AutoSize = true;
            this.bordersCB.Location = new System.Drawing.Point(198, 102);
            this.bordersCB.Name = "bordersCB";
            this.bordersCB.Size = new System.Drawing.Size(15, 14);
            this.bordersCB.TabIndex = 15;
            this.bordersCB.UseVisualStyleBackColor = true;
            this.bordersCB.CheckedChanged += new System.EventHandler(this.bordersCB_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(161, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "draw Borders between Desktops";
            // 
            // aeroCB
            // 
            this.aeroCB.AutoSize = true;
            this.aeroCB.Location = new System.Drawing.Point(198, 82);
            this.aeroCB.Name = "aeroCB";
            this.aeroCB.Size = new System.Drawing.Size(15, 14);
            this.aeroCB.TabIndex = 10;
            this.aeroCB.UseVisualStyleBackColor = true;
            this.aeroCB.CheckedChanged += new System.EventHandler(this.aeroCB_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Aero Background";
            // 
            // EmphasizeDesktopCB
            // 
            this.EmphasizeDesktopCB.FormattingEnabled = true;
            this.EmphasizeDesktopCB.Items.AddRange(new object[] {
            "emphasize with borders",
            "emphasize with smooth background",
            "don\'t emphasize"});
            this.EmphasizeDesktopCB.Location = new System.Drawing.Point(198, 26);
            this.EmphasizeDesktopCB.Name = "EmphasizeDesktopCB";
            this.EmphasizeDesktopCB.Size = new System.Drawing.Size(207, 21);
            this.EmphasizeDesktopCB.TabIndex = 8;
            this.EmphasizeDesktopCB.SelectedIndexChanged += new System.EventHandler(this.EmphasizeDesktopCB_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "emphasize active Desktop";
            // 
            // HotKeysPage
            // 
            this.HotKeysPage.AutoScroll = true;
            this.HotKeysPage.Location = new System.Drawing.Point(4, 22);
            this.HotKeysPage.Name = "HotKeysPage";
            this.HotKeysPage.Padding = new System.Windows.Forms.Padding(3);
            this.HotKeysPage.Size = new System.Drawing.Size(649, 262);
            this.HotKeysPage.TabIndex = 1;
            this.HotKeysPage.Text = "Hotkeys";
            this.HotKeysPage.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.hideAllWindowsBtn);
            this.tabPage2.Controls.Add(this.showAllWindowsBtn);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(649, 262);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "Advanced";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // hideAllWindowsBtn
            // 
            this.hideAllWindowsBtn.Location = new System.Drawing.Point(24, 108);
            this.hideAllWindowsBtn.Name = "hideAllWindowsBtn";
            this.hideAllWindowsBtn.Size = new System.Drawing.Size(122, 23);
            this.hideAllWindowsBtn.TabIndex = 5;
            this.hideAllWindowsBtn.Text = "Hide visible windows";
            this.hideAllWindowsBtn.UseVisualStyleBackColor = true;
            this.hideAllWindowsBtn.Click += new System.EventHandler(this.hideAllWindowsBtn_Click);
            // 
            // showAllWindowsBtn
            // 
            this.showAllWindowsBtn.Location = new System.Drawing.Point(24, 79);
            this.showAllWindowsBtn.Name = "showAllWindowsBtn";
            this.showAllWindowsBtn.Size = new System.Drawing.Size(122, 23);
            this.showAllWindowsBtn.TabIndex = 4;
            this.showAllWindowsBtn.Text = "Show hidden windows";
            this.showAllWindowsBtn.UseVisualStyleBackColor = true;
            this.showAllWindowsBtn.Click += new System.EventHandler(this.showAllWindowsBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(616, 39);
            this.label3.TabIndex = 3;
            this.label3.Text = resources.GetString("label3.Text");
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(590, 312);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 1;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(21, 93);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(130, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "unhide all windows on exit";
            // 
            // unhideOnExitCB
            // 
            this.unhideOnExitCB.AutoSize = true;
            this.unhideOnExitCB.Location = new System.Drawing.Point(198, 92);
            this.unhideOnExitCB.Name = "unhideOnExitCB";
            this.unhideOnExitCB.Size = new System.Drawing.Size(15, 14);
            this.unhideOnExitCB.TabIndex = 18;
            this.unhideOnExitCB.UseVisualStyleBackColor = true;
            this.unhideOnExitCB.CheckedChanged += new System.EventHandler(this.unhideOnExitCB_CheckedChanged);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 347);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.AdvancedTb);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.AdvancedTb.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.aboutImg)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DesktopCountUD)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpdateRateTB)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl AdvancedTb;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage HotKeysPage;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ThumbnailViewCB;
        private System.Windows.Forms.NumericUpDown DesktopCountUD;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label updateRateLabel2;
        private System.Windows.Forms.TrackBar UpdateRateTB;
        private System.Windows.Forms.Label UpdateRateLabel;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button hideAllWindowsBtn;
        private System.Windows.Forms.Button showAllWindowsBtn;
        private System.Windows.Forms.ComboBox EmphasizeDesktopCB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox aeroCB;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox tooltipCB;
        private System.Windows.Forms.CheckBox bordersCB;
        private System.Windows.Forms.CheckBox addCloseCB;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.PictureBox aboutImg;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button previewHeightBtn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox startingWithWindowsCB;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox unhideOnExitCB;
        private System.Windows.Forms.Label label11;
    }
}