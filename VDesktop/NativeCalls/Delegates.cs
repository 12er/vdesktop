﻿using System;
using System.Collections.Generic;
using System.Text;

/**
 * Some delegates. This collection of WFC api calls is based on the
 * NativeCalls classes from SharpDesktop.
 * Refer to http://www.pinvoke.net for details.
 */
namespace VDesktop.NativeCalls
{
    public delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam);
    public delegate int HookProc(int code, IntPtr wParam, IntPtr lParam);
}
