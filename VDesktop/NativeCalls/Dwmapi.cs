﻿using System;
using System.Runtime.InteropServices;
using System.Text;

/**
 * Partial interface from Dwmapi.dll used by this app.
 */
namespace VDesktop.NativeCalls
{
    class Dwmapi
    {

        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern int DwmRegisterThumbnail(IntPtr destinationWindowHandle, IntPtr sourceWindowHandle, out IntPtr thumbnailHandle);

        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern int DwmUnregisterThumbnail(IntPtr thumbnailHandle);

        [DllImport("dwmapi.dll")]
        public static extern int DwmQueryThumbnailSourceSize(IntPtr thumb, out PSIZE size);

        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern void DwmUpdateThumbnailProperties(IntPtr thumbnailHandle, ref DWM_THUMBNAIL_PROPERTIES properties);

        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern int DwmExtendFrameIntoClientArea(IntPtr hwnd, ref MARGINS margins);

        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();

        [DllImport("dwmapi.dll")]
        public static extern void DwmEnableBlurBehindWindow(IntPtr hwnd, ref DWM_BLURBEHIND blurBehind);

    }
}
