﻿using System;
using System.Runtime.InteropServices;
using System.Text;

/**
 * Partial interface from the Kernel32.dll used by this app.
 * This collection of WFC api calls is based on the
 * NativeCalls classes from SharpDesktop.
 * Refer to http://www.pinvoke.net for details.
 */
namespace VDesktop.NativeCalls
{
    public class Kernel32
    {

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool AllocConsole();

        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        static extern IntPtr GetModuleHandle(string moduleName);

    }
}
