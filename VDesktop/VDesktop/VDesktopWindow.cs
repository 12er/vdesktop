﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDesktop.NativeCalls;
using System.Drawing;
using System.Threading;

namespace VDesktop
{
    public class VDesktopWindow
    {

        private IntPtr hWnd;
        private int zOrder;
        private bool visible;
        private bool visibleVarUsed;
        private Icon icon;

        public VDesktopWindow(IntPtr hWnd)
        {
            this.hWnd = hWnd;
            this.zOrder = 0;
            this.visible = false;
            this.visibleVarUsed = false;
            icon = null;
        }

        public IntPtr GetHandle()
        {
            return hWnd;
        }

        public bool IsValid()
        {
            if (hWnd != IntPtr.Zero && User32.IsWindow(hWnd))
            {
                return true;
            }
            return false;
        }

        public int GetZOrder()
        {
            return zOrder;
        }

        public void SetZOrder(int zOrder)
        {
            this.zOrder = zOrder;
        }

        public bool IsVisible()
        {
            if (visibleVarUsed)
            {
                return visible;
            }
            else
            {
                if (User32.IsWindowVisible(hWnd))
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsDesktopWindow()
        {
            if (!IsValid())
            {
                return false;
            }
            int style = (int)User32.GetWindowLongPtr(hWnd, GWL.GWL_STYLE).ToInt64();
            int exstyle = (int)User32.GetWindowLongPtr(hWnd, GWL.GWL_EXSTYLE).ToInt64();
            if (
                (exstyle & (uint)WindowStylesEx.WS_EX_APPWINDOW) != 0 ||
                (style & (uint)WindowStyles.WS_OVERLAPPED) != 0 ||
                (exstyle & (uint)WindowStylesEx.WS_EX_OVERLAPPEDWINDOW) != 0
                )
            {
                return true;
            }
            return false;
        }

        public void Show()
        {
            if (IsValid())
            {
                visible = true;
                visibleVarUsed = true;
                User32.ShowWindowAsync(hWnd, WindowShowStyle.Show);
                Thread.Sleep(10);
            }
        }

        public void Hide()
        {
            if (IsValid())
            {
                visible = false;
                visibleVarUsed = true;
                User32.ShowWindowAsync(hWnd, WindowShowStyle.Hide);
                Thread.Sleep(10);
            }
        }

        public String GetTitle()
        {
            if(!IsValid())
            {
                return "";
            }
            int length = User32.GetWindowTextLength(hWnd);
            if (length < 1)
            {
                return "";
            }
            StringBuilder builder = new StringBuilder(length + 1);
            User32.GetWindowText(hWnd, builder, builder.Capacity);
            return builder.ToString();
        }

        public Icon GetIcon()
        {
            if (icon != null)
            {
                return icon;
            }

            const int WM_GETICON = 0x7F;
            const int ICON_SMALL2 = 2;
            const int ICON_SMALL = 0;
            const int ICON_BIG = 1;
            IntPtr iconHandle = IntPtr.Zero;

            User32.SendMessageTimeout(hWnd, WM_GETICON, new IntPtr(ICON_SMALL2), new IntPtr(0), SendMessageTimeoutFlags.SMTO_ABORTIFHUNG, 1, out iconHandle);

            if (iconHandle == IntPtr.Zero)
            {
                User32.SendMessageTimeout(hWnd, WM_GETICON, new IntPtr(ICON_SMALL), new IntPtr(0), SendMessageTimeoutFlags.SMTO_ABORTIFHUNG, 1, out iconHandle);
            }
            if (iconHandle == IntPtr.Zero)
            {
                User32.SendMessageTimeout(hWnd, WM_GETICON, new IntPtr(ICON_BIG), new IntPtr(0), SendMessageTimeoutFlags.SMTO_ABORTIFHUNG, 1, out iconHandle);
            }
            if (iconHandle == IntPtr.Zero)
            {
                iconHandle = User32.GetClassLongPtr(hWnd, GetClassLongPtr.GCL_HICON);
            }
            if (iconHandle == IntPtr.Zero)
            {
                iconHandle = User32.GetClassLongPtr(hWnd, GetClassLongPtr.GCL_HICONSM);
            }

            if (iconHandle == IntPtr.Zero)
            {
                return null;
            }

            icon = Icon.FromHandle(iconHandle);
            if (icon.Width <= 0 || icon.Height <= 0)
            {
                icon = null;
            }
            return icon;
        }

        public Rectangle GetRectangle()
        {
            if (!IsValid())
            {
                return new Rectangle();
            }
            RECT r = new RECT();
            User32.GetWindowRect(hWnd, ref r);
            return new Rectangle(r.left, r.top, r.right - r.left, r.bottom - r.top);
        }

    }
}
