﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;

namespace VDesktop
{

    public class VGraphics
    {

        public static float[] GetGaussFilter(float sigma)
        {
            float maxLocation = (float)Math.Floor(2.5f * sigma);
            int size = (int)Math.Floor(2.0f * maxLocation) + 1;
            float[] kernel = new float[size];
            for (float l = -maxLocation; l <= (int)maxLocation; l++)
            {
                int i = (int)(l + maxLocation);
                kernel[i] = (float)((1.0 / (sigma * Math.Sqrt(2.0 * Math.PI))) * Math.Exp(-0.5 * Math.Pow(l / sigma, 2.0)));
            }
            return kernel;
        }

        public static Bitmap BlurImage(Bitmap orig, float sigma)
        {
            Bitmap pass1 = new Bitmap(orig.Width, orig.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Bitmap pass2 = new Bitmap(orig.Width, orig.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            float[] gaussFilter = GetGaussFilter(sigma);
            int pixelDelta = (int)Math.Floor((double)gaussFilter.Length / 2.0);
            int netCacheSize = 10;

            //blur horizontally
            float[] blurACache = new float[netCacheSize + 2 * pixelDelta];
            float[] blurRCache = new float[netCacheSize + 2 * pixelDelta];
            float[] blurGCache = new float[netCacheSize + 2 * pixelDelta];
            float[] blurBCache = new float[netCacheSize + 2 * pixelDelta];
            for (int h = 0; h < orig.Height; h++)
            {
                for (int w = 0; w < orig.Width; w++)
                {
                    int moduloClass = w % netCacheSize;
                    //time to fill the caches for the current row
                    if (moduloClass == 0)
                    {
                        for (int cachePointer = 0; cachePointer < netCacheSize + 2 * pixelDelta; cachePointer++)
                        {
                            int coord = Math.Min(Math.Max(cachePointer + w - pixelDelta, 0), orig.Width - 1);
                            Color p = orig.GetPixel(coord, h);
                            blurACache[cachePointer] = p.A;
                            blurRCache[cachePointer] = p.R;
                            blurGCache[cachePointer] = p.G;
                            blurBCache[cachePointer] = p.B;
                        }
                    }
                    //deploy gaussian filter at current location
                    //use autocorrelation instead of convolution, as the filter is symmetric
                    float a = 0;
                    float r = 0;
                    float g = 0;
                    float b = 0;
                    for (int foldLoc = 0; foldLoc < gaussFilter.Length; foldLoc++)
                    {
                        int index = foldLoc + moduloClass;
                        a += gaussFilter[foldLoc] * blurACache[index];
                        r += gaussFilter[foldLoc] * blurRCache[index];
                        g += gaussFilter[foldLoc] * blurGCache[index];
                        b += gaussFilter[foldLoc] * blurBCache[index];
                    }
                    pass1.SetPixel(w, h, Color.FromArgb((byte)a, (byte)r, (byte)g, (byte)b));
                }
            }

            //blur vertically
            for (int w = 0; w < orig.Width; w++)
            {
                for (int h = 0; h < orig.Height; h++)
                {
                    int moduloClass = h % netCacheSize;
                    if (moduloClass == 0)
                    {
                        //time to fill the caches for another time
                        for (int cachePointer = 0; cachePointer < netCacheSize + 2 * pixelDelta; cachePointer++)
                        {
                            int coord = Math.Min(Math.Max(cachePointer + h - pixelDelta, 0), orig.Height - 1);
                            Color p = pass1.GetPixel(w, coord);
                            blurACache[cachePointer] = p.A;
                            blurRCache[cachePointer] = p.R;
                            blurGCache[cachePointer] = p.G;
                            blurBCache[cachePointer] = p.B;
                        }
                    }
                    //deploy gaussian filter at current location
                    //use autocorrelation instead of convolution, as the filter is symmetric
                    float a = 0;
                    float r = 0;
                    float g = 0;
                    float b = 0;
                    for (int foldLoc = 0; foldLoc < gaussFilter.Length; foldLoc++)
                    {
                        int index = foldLoc + moduloClass;
                        a += gaussFilter[foldLoc] * blurACache[index];
                        r += gaussFilter[foldLoc] * blurRCache[index];
                        g += gaussFilter[foldLoc] * blurGCache[index];
                        b += gaussFilter[foldLoc] * blurBCache[index];
                    }
                    pass2.SetPixel(w, h, Color.FromArgb((byte)a, (byte)r, (byte)g, (byte)b));
                }
            }

            pass1.Dispose();
            return pass2;
        }

    }

}
