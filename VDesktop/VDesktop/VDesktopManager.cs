﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VDesktop
{
    public class VDesktopManager
    {

        private static VDesktopManager VDmanager = null;

        public static VDesktopManager Get()
        {
            if (VDmanager == null)
            {
                Load();
            }
            return VDmanager;
        }

        public static void Load()
        {
            VDmanager = new VDesktopManager();
            int desktopCount = Settings.Get().GetDesktopCount();
            for (int i = 1; i <= desktopCount; i++)
            {
                VDmanager.AddDesktop((uint)i);
            }
        }

        public static void Save()
        {
            if (VDmanager != null)
            {
                int desktopCount = VDmanager.desktops.Count();
                Settings.Get().SetDesktopCount(desktopCount);
            }
        }

        private VDesktop currentDesktop = null;
        private List<VDesktop> desktops = null;
        private UDictionary<IntPtr, VDesktopWindow> ignoredWindows = null;

        public VDesktopManager()
        {
            ignoredWindows = new UDictionary<IntPtr, VDesktopWindow>();
            desktops = new List<VDesktop>();
        }

        public UDictionary<IntPtr, VDesktopWindow> GetIgnoredWindows()
        {
            return ignoredWindows;
        }

        public List<VDesktop> GetDesktops()
        {
            return desktops;
        }

        public VDesktop GetCurrentDesktop()
        {
            return currentDesktop;
        }

        public void AddDesktop(uint ID)
        {
            foreach (VDesktop d in desktops)
            {
                if (d.getID() == ID)
                {
                    //nothing to do
                    //desktop with ID already exists
                    return;
                }
            }
            VDesktop desktop = new VDesktop(ID);
            if (currentDesktop == null)
            {
                currentDesktop = desktop;
            }
            desktops.Add(desktop);
            Save();
        }

        public void RemoveDesktop(uint ID)
        {
            VDesktop desk = null;
            VDesktop electDifferent = null;
            foreach (VDesktop d in desktops)
            {
                if (d.getID() == ID)
                {
                    desk = d;
                }
                else
                {
                    electDifferent = d;
                }
            }
            if (desk == null)
            {
                //desktop does not exist
                //nothing to do
                return;
            }
            if (currentDesktop.getID() == ID)
            {
                //the currently active desktop
                //is removed, hence elect
                //another active desktop
                currentDesktop = electDifferent;
            }
            if (currentDesktop != null)
            {
                currentDesktop.Show();
            }
            desk.Dispose();
            desktops.Remove(desk);
            Save();
        }

        public void Show(uint ID)
        {
            if(currentDesktop != null && currentDesktop.getID() == ID) {
                //nothing changed
                //nothing to do
                return;
            }
            
            VDesktop desk = null;
            foreach (VDesktop d in desktops)
            {
                if (d.getID() == ID)
                {
                    desk = d;
                }
            }
            if (desk == null)
            {
                //a desktop with ID does
                //not exist, nothing to do
                return;
            }
            if (currentDesktop != null)
            {
                //the content of the current
                //desktop is hidden
                currentDesktop.Hide();
                
            }
            currentDesktop = desk;
            currentDesktop.Show();
        }

        public void Dispose()
        {
            foreach (VDesktop d in desktops)
            {
                d.Dispose();
            }
            desktops.Clear();
        }

    }
}
