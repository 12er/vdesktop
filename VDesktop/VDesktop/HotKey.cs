﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using VDesktop.NativeCalls;

namespace VDesktop
{
    [Flags]
    public enum KeyModifier
    {
        KM_ALT = 1,
        KM_CTRL = 2,
        KM_SHIFT = 4,
        KM_WINDOWS = 8,
        KM_NOREPEAT = 0x4000
    }

    public class HotKeyEventArgs : EventArgs
    {
        public readonly Keys key;
        public readonly KeyModifier keyModifier;

        public HotKeyEventArgs(Keys key, KeyModifier keyModifier)
        {
            this.key = key;
            this.keyModifier = keyModifier;
        }

        public HotKeyEventArgs(IntPtr param)
        {
            uint p = (uint)param.ToInt64();
            key = (Keys)((p & 0xffff0000) >> 16);
            keyModifier = (KeyModifier)(p & 0x0000ffff);
        }

    }

    public struct HotKey
    {
        public Keys key;
        public KeyModifier keyModifier;
    }

    public class HotKeyManager
    {
        private static HotKeyManager manager = null;

        public static HotKeyManager Get()
        {
            if (manager == null)
            {
                manager = new HotKeyManager("HotKeyManagerThread");
            }
            return manager;
        }

        public event EventHandler<HotKeyEventArgs> HotKeyDown;
        public event EventHandler<HotKeyEventArgs> HotKeyPressed;
        public event EventHandler<HotKeyEventArgs> HotKeyUp;
        private volatile HotKeyForm hotKeyForm;
        private int nextID = 0;
        private Dictionary<int, HotKey> registeredKeys = new Dictionary<int, HotKey>();
        private Dictionary<uint, uint> pressedKeys = new Dictionary<uint, uint>();
        private ManualResetEvent syncForm = new ManualResetEvent(false);

        private HotKeyManager(String Name)
        {
            hotKeyForm = new HotKeyForm(this);
            Thread messageLoop = new Thread(
                delegate()
                {
                    
                    Application.Run(hotKeyForm);
                }
                );
            messageLoop.Name = Name;
            messageLoop.IsBackground = true;
            messageLoop.Start();
        }

        public int RegisterHotKey(Keys key, KeyModifier keyModifier)
        {
            syncForm.WaitOne();
            int ID = System.Threading.Interlocked.Increment(ref nextID);
            hotKeyForm.Invoke(new RegisterHotKeyDelegate(RegisterHotKeyInternal), hotKeyForm.Handle, ID, (uint)keyModifier, (uint)key);

            return ID;
        }

        public void UnregisterHotKey(int ID)
        {
            hotKeyForm.Invoke(new UnregisterHotKeyDelegate(UnregisterHotKeyInternal), hotKeyForm.Handle, ID);
        }

        public void UnregisterHotKeys()
        {
            Dictionary<int, HotKey> copiedRegs = new Dictionary<int, HotKey>();
            lock (registeredKeys)
            {
                foreach (KeyValuePair<int, HotKey> p in registeredKeys)
                {
                    copiedRegs[p.Key] = p.Value;
                }
            }
            foreach (KeyValuePair<int, HotKey> p in copiedRegs)
            {
                UnregisterHotKey(p.Key);
            }
        }

        private void OnHotKeyDown(HotKeyEventArgs e)
        {
            if (HotKeyDown != null)
            {
                HotKeyDown(null, e);
            }
        }

        private void OnHotKeyPressed(HotKeyEventArgs e)
        {
            if (HotKeyPressed != null)
            {
                HotKeyPressed(null, e);
            }
        }

        private void OnHotKeyUp(HotKeyEventArgs e)
        {
            if (HotKeyUp != null)
            {
                HotKeyUp(null, e);
            }
        }

        delegate void RegisterHotKeyDelegate(IntPtr hwnd, int id, uint modifiers, uint key);

        private void RegisterHotKeyInternal(IntPtr hwnd, int id, uint modifiers, uint key)
        {
            bool success = User32.RegisterHotKey(hwnd, id, modifiers, key);
            if (success)
            {
                lock (registeredKeys)
                {
                    if (!registeredKeys.ContainsKey(id))
                    {
                        HotKey s = new HotKey();
                        s.key = (Keys)key;
                        s.keyModifier = (KeyModifier)modifiers;
                        registeredKeys[id] = s;
                    }
                }
            }
        }

        delegate void UnregisterHotKeyDelegate(IntPtr hwnd, int id);

        private void UnregisterHotKeyInternal(IntPtr hwnd, int ID)
        {
            User32.UnregisterHotKey(hwnd, ID);
            lock (registeredKeys)
            {
                if (registeredKeys.ContainsKey(ID))
                {
                    registeredKeys.Remove(ID);
                }
            }
        }

        private class HotKeyForm : Form
        {
            private HotKeyManager manager;

            public HotKeyForm(HotKeyManager manager)
            {
                this.manager = manager;
                manager.syncForm.Set();
            }

            protected override void WndProc(ref Message m)
            {
                if (m.Msg == User32.WmHotKey)
                {
                    uint p = (uint)m.LParam.ToInt64();
                    HotKeyEventArgs e = new HotKeyEventArgs(m.LParam);

                    //if key hasn't been pressed yet,
                    //mark it as pressed, and send a keydown event
                    lock (manager.pressedKeys)
                    {
                        if (!manager.pressedKeys.ContainsKey(p))
                        {
                            manager.OnHotKeyDown(e);
                            manager.pressedKeys[p] = p;
                            KeyTimer t = new KeyTimer(manager, p, 100);
                            t.Start();
                        }
                    }

                    manager.OnHotKeyPressed(e);
                }

                base.WndProc(ref m);
            }

            private class KeyTimer : System.Windows.Forms.Timer
            {

                private HotKeyManager manager;
                private uint key;

                public KeyTimer(HotKeyManager manager, uint key, int millies)
                {
                    this.manager = manager;
                    this.key = key;
                    this.Interval = millies;
                    Tick += new EventHandler(CheckKeypressed);
                }

                private void CheckKeypressed(object o, EventArgs e)
                {
                    Stop();
                    //continue polling, if key remains pressed
                    //otherwise stop timer, and send release message
                    lock (manager.pressedKeys)
                    {
                        if (HasKey())
                        {
                            Enabled = true;
                        }
                        else
                        {
                            if (manager.pressedKeys.ContainsKey(key))
                            {
                                manager.pressedKeys.Remove(key);
                                Keys pureKey = (Keys)((key & 0xffff0000) >> 16);
                                KeyModifier keyModifier = (KeyModifier)(key & 0x0000ffff);
                                HotKeyEventArgs ev = new HotKeyEventArgs(pureKey, keyModifier);
                                manager.OnHotKeyUp(ev);
                            }
                            Enabled = false;
                        }
                    }
                }

                private bool HasKey()
                {
                    //check which modifiers are pressed
                    bool altPressed = Convert.ToBoolean(User32.GetAsyncKeyState((int)Keys.Menu));
                    bool ctrlPressed = Convert.ToBoolean(User32.GetAsyncKeyState((int)Keys.ControlKey));
                    bool shiftPressed = Convert.ToBoolean(User32.GetAsyncKeyState((int)Keys.ShiftKey));
                    bool winPressed = Convert.ToBoolean(User32.GetAsyncKeyState((int)Keys.LWin)) || Convert.ToBoolean(User32.GetAsyncKeyState((int)Keys.RWin));

                    //check if the non-modifier part of key is pressed
                    uint pureKey = ((key & 0xffff0000) >> 16);
                    bool keyPressed = Convert.ToBoolean(User32.GetAsyncKeyState((int)pureKey));

                    //get modifiers from key
                    uint modKey = key & 0xffff;
                    bool altRequired = (modKey & (uint)KeyModifier.KM_ALT) != 0;
                    bool ctrlRequired = (modKey & (uint)KeyModifier.KM_CTRL) != 0;
                    bool shiftRequired = (modKey & (uint)KeyModifier.KM_SHIFT) != 0;
                    bool winRequired = (modKey & (uint)KeyModifier.KM_WINDOWS) != 0;

                    //perform a check, if key is pressed
                    if (
                        ((altRequired & altPressed) == altRequired) &&
                        ((ctrlRequired & ctrlPressed) == ctrlRequired) &&
                        ((shiftRequired & shiftPressed) == shiftRequired) &&
                        ((winRequired & winPressed) == winRequired) &&
                        keyPressed == true
                        )
                    {
                        return true;
                    }

                    return false;
                }
            }

            protected override void SetVisibleCore(bool value)
            {
                base.SetVisibleCore(false);
            }
        }
    }

}
