﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VDesktop.NativeCalls;
using System.Windows.Interop;

namespace VDesktop
{
    public class VDesktop
    {

        private uint ID;
        private UDictionary<IntPtr, VDesktopWindow> windows;
        private UDictionary<IntPtr, VDesktopWindow> oldWindows;
        private static int zOrder;

        public VDesktop(uint ID)
        {
            this.ID = ID;
            windows = new UDictionary<IntPtr, VDesktopWindow>();
            oldWindows = new UDictionary<IntPtr, VDesktopWindow>();
        }

        public uint getID()
        {
            return ID;
        }

        public UDictionary<IntPtr, VDesktopWindow> GetWindows()
        {
            return windows;
        }

        public List<VDesktopWindow> GetOrderedWindows()
        {
            return windows.Values.OrderByDescending(w=>w.GetZOrder()).ToList();
        }

        public void UpdateWindows()
        {
            //remember old VDesktopWindow objects
            oldWindows.Clear();
            foreach (IntPtr hWnd in windows.Keys)
            {
                oldWindows[hWnd] = windows[hWnd];
            }

            //collect windows
            windows.Clear();
            zOrder = 0;
            User32.EnumWindows(EnumWindowsProc, IntPtr.Zero);

            //associate the oldest VDesktopWindow object with the corresponding hWnd
            foreach (IntPtr hWnd in oldWindows.Keys)
            {
                if (windows.ContainsKey(hWnd))
                {
                    windows[hWnd] = oldWindows[hWnd];
                }
            }
        }

        private bool EnumWindowsProc(IntPtr hWnd, IntPtr lParams)
        {
            VDesktopManager vdm = VDesktopManager.Get();
            UDictionary<IntPtr, VDesktopWindow> ignoredWindows = vdm.GetIgnoredWindows();

            VDesktopWindow window = new VDesktopWindow(hWnd);
            if (oldWindows.ContainsKey(hWnd))
            {
                window = oldWindows[hWnd];
            }
            window.SetZOrder(zOrder++);
            if (window.IsDesktopWindow() && window.IsVisible() && !ignoredWindows.ContainsKey(hWnd))
            {
                //adding a window to this desktop?
                //it may already be on another desktop
                //if that's the case, don't insert it
                //and don't show it
                bool onThisDesktop = true;
                foreach (VDesktop d in vdm.GetDesktops())
                {
                    if (d.GetWindows().ContainsKey(hWnd))
                    {
                        d.GetWindows()[hWnd].Hide();
                        d.GetWindows()[hWnd].SetZOrder(window.GetZOrder());
                        onThisDesktop = false;
                    }
                }
                if (onThisDesktop)
                {
                    windows.Update(hWnd, window);
                }
            }

            return true;
        }

        public void InsertHiddenWindows()
        {
            VDesktopManager vdm = VDesktopManager.Get();

            //remove windows on this desktop
            //store the windows of the other desktops
            //in a dictionary to avoid showing those on this desktop
            oldWindows.Clear();
            foreach (VDesktop vdesk in vdm.GetDesktops())
            {
                if (vdesk == vdm.GetCurrentDesktop())
                {
                    continue;
                }
                foreach (IntPtr hWnd in vdesk.GetWindows().Keys)
                {
                    oldWindows[hWnd] = vdesk.GetWindows()[hWnd];
                }
            }

            //collect windows
            windows.Clear();
            zOrder = 0;
            User32.EnumWindows(EnumHiddenWindowsProc, IntPtr.Zero);
        }

        private bool EnumHiddenWindowsProc(IntPtr hWnd, IntPtr lParams)
        {
            VDesktopManager vdm = VDesktopManager.Get();
            UDictionary<IntPtr, VDesktopWindow> ignoredWindows = vdm.GetIgnoredWindows();

            //it's a valid window not on the other desktops?
            //Show it, and insert it in current desktop
            VDesktopWindow window = new VDesktopWindow(hWnd);
            window.SetZOrder(zOrder++);
            if (window.IsDesktopWindow() && !ignoredWindows.ContainsKey(hWnd) && !oldWindows.ContainsKey(hWnd))
            {
                window.Show();
                windows[hWnd] = window;
            }

            return true;
        }

        public void HideAndRemoveWindows()
        {
            foreach (VDesktopWindow window in windows.Values)
            {
                if (!window.IsValid())
                {
                    continue;
                }
                window.Hide();
            }
            windows.Clear();
        }

        public void Show()
        {
            List<VDesktopWindow> orderedWindows = GetOrderedWindows();
            foreach (VDesktopWindow w in orderedWindows)
            {
                w.Show();
            }
        }

        public void Hide()
        {
            //track windows
            //in order to show those
            //the next time this desktop is
            //chosen
            UpdateWindows();
            try
            {
                foreach (VDesktopWindow w in windows.Values)
                {
                    w.Hide();
                }
            }
            catch (Exception)
            {
            }
        }

        public void Dispose()
        {
            if (Settings.Get().GetUseUnhideOnExit())
            {
                Show();
            }
            windows.Clear();
        }

    }

}
