﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VDesktop
{
    public class UDictionary<TKey, TValue> : Dictionary<TKey, TValue>
    {

        public UDictionary(): base()
        {
        }

        public void Update(TKey key, TValue value)
        {
            if (ContainsKey(key))
            {
                Remove(key);
            }
            Add(key, value);
        }

    }
}
