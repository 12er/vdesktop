﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Win32;

namespace VDesktop
{

    public enum ThumbnailView
    {
        CachedAeroWindows,
        AeroWindows,
        BasicWindows
    }

    public enum EmphasizeDesktop
    {
        BorderedEmphasizer,
        SmoothEmphasizer,
        NoEmphasizer
    }

    public class Settings
    {

        private static Settings settings = null;

        public static Settings Get()
        {
            if (settings == null)
            {
                settings = new Settings();
            }
            return settings;
        }

        public static String GetSettingsDirectory()
        {
            String path = Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)).FullName;
            if (Environment.OSVersion.Version.Major >= 6)
            {
                path = Directory.GetParent(path).ToString();
            }
            path += "\\.VDesktop";
            return path;
        }

        public static void TouchSettingsDirectory()
        {
            String dir = GetSettingsDirectory();
            if (!Directory.Exists(dir))
            {
                try
                {
                    Directory.CreateDirectory(dir);
                }
                catch (Exception)
                {
                }
            }
        }

        public static readonly String CachedAeroWindows = "Cached Aero Windows";
        public static readonly String AeroWindows = "Aero Windows";
        public static readonly String BasicWindows = "Basic Windows";

        public static readonly String BorderedEmphasizer = "emphasize with borders";
        public static readonly String SmoothEmphasizer = "emphasize with smooth background";
        public static readonly String NoEmphasizer = "don't emphasize";

        private int desktopCount = 1;
        private int previewHeight = 150;
        private bool startingWithWindows = false;
        private bool useAero = true;
        private bool useAddCloseButtons = true;
        private bool useUnhideOnExit = true;
        private bool useBorders = true;
        private bool useTooltip = true;
        private ThumbnailView thumbnailView = ThumbnailView.CachedAeroWindows;
        private EmphasizeDesktop emphasizeDesktop = EmphasizeDesktop.BorderedEmphasizer;
        private double updateThumbnailRate = 0.3;
        private List<HotKey> keyShow = new List<HotKey>();
        private List<HotKey> keyMoveLeft = new List<HotKey>();
        private List<HotKey> keyMoveRight = new List<HotKey>();
        private List<HotKey> keyLeft = new List<HotKey>();
        private List<HotKey> keyRight = new List<HotKey>();

        private bool readSettings = false;
        private Mutex mutex = new Mutex();

        private Settings()
        {
        }

        public int GetDesktopCount()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return Math.Max(desktopCount, 1);
        }

        public void SetDesktopCount(int c)
        {
            desktopCount = Math.Max(1, c);
            WriteSettings();
        }

        public int GetPreviewHeight()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return Math.Max(previewHeight, 10);
        }

        public void SetPreviewHeight(int height)
        {
            previewHeight = Math.Max(height, 10);
            WriteSettings();
        }

        public bool GetStartingWithWindows()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return startingWithWindows;
        }

        public void SetStartingWithWindows(bool starting)
        {
            startingWithWindows = starting;
            if (starting)
            {
                AddStartWithWindows();
            }
            else
            {
                RemoveStartWithWindows();
            }
            WriteSettings();
        }

        private void AddStartWithWindows()
        {
            //create XML file for schtasks.exe
            String userID = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            String xmlText = 
                "<?xml version=\"1.0\" ?>\n" +
                "<Task xmlns=\"http://schemas.microsoft.com/windows/2004/02/mit/task\">\n" +
                "    <RegistrationInfo>\n" +
                "        <Date>2015-10-29T03:00:00</Date>\n" +
                "        <Author>Tristan Bauer</Author>\n" +
                "        <Version>1.0.0</Version>\n" +
                "        <Description>Virtual Desktop</Description>\n" +
                "    </RegistrationInfo>\n" +
                "    <Triggers>\n" +
	            "    <LogonTrigger>\n" +
	            "        <StartBoundary>2015-10-11T13:21:17-08:00</StartBoundary>\n" +
                "            <EndBoundary>2115-01-01T00:00:00-08:00</EndBoundary>\n" +
	            "        <Enabled>true</Enabled>\n" +
	            "        <UserId>" + userID + "</UserId>\n" +
	            "    </LogonTrigger>\n" +
                "    </Triggers>\n" +
                "    <Principals>\n" +
                "        <Principal>\n" +
		        "        <UserId>" + userID + "</UserId>\n" +
		        "        <LogonType>InteractiveToken</LogonType>\n" +
		        "        <RunLevel>HighestAvailable</RunLevel>\n" +
                "        </Principal>\n" +
                "    </Principals>\n" +
                "    <Settings>\n" +
                "        <Enabled>true</Enabled>\n" +
                "        <AllowStartOnDemand>true</AllowStartOnDemand>\n" +
                "        <AllowHardTerminate>true</AllowHardTerminate>\n" +
                "    </Settings>\n" +
                "    <Actions>\n" +
                "        <Exec>\n" +
                "            <Command>" + System.Reflection.Assembly.GetExecutingAssembly().Location + "</Command>\n" +
                "        <WorkingDirectory>" + System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "</WorkingDirectory>\n" +
                "        </Exec>\n" +
                "    </Actions>\n" +
                "</Task>\n"
                ;
            FileStream s = null;
            String filePath = "";
            try
            {
                System.IO.File.WriteAllText("./Task.xml", xmlText);
                filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Task.xml";
            }
            catch (Exception)
            {
            }

            try
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.FileName = "schtasks.exe";
                process.StartInfo.Arguments = " /Create /TN VDesktop" + ToAlphaNumeric(userID) + " /XML \"" + filePath + "\" /f";
                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                process.StartInfo.Verb = "runas";
                process.Start();
                process.WaitForExit();
            }
            catch (Exception)
            {
            }
        }

        private void RemoveStartWithWindows()
        {
            try
            {
                String userID = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.FileName = "schtasks.exe";
                process.StartInfo.Arguments = " /DELETE /TN VDesktop" + ToAlphaNumeric(userID) + " /f";
                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                process.StartInfo.Verb = "runas";
                process.Start();
                process.WaitForExit();
            }
            catch (Exception)
            {
            }
        }

        private static String ToAlphaNumeric(String str)
        {
            StringBuilder outStr = new StringBuilder(str.Length);
            for (int i = 0; i < str.Length; i++)
            {
                char c = str[i];
                if (!CharIsAlphaNumeric(c))
                {
                    c = '_';
                }
                outStr.Append(c);
            }
            return outStr.ToString();
        }

        private static bool CharIsAlphaNumeric(char c)
        {
            if (c >= '0' && c <= '9')
            {
                return true;
            }
            if (c >= 'a' && c <= 'z')
            {
                return true;
            }
            if (c >= 'A' && c <= 'Z')
            {
                return true;
            }
            return false;
        }

        public bool GetUseAero()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return useAero;
        }

        public void SetUseAero(bool use)
        {
            useAero = use;
            WriteSettings();
        }

        public bool GetUseAddCloseButtons()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return useAddCloseButtons;
        }

        public void SetUseAddCloseButtons(bool use)
        {
            useAddCloseButtons = use;
            WriteSettings();
        }

        public bool GetUseUnhideOnExit()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return useUnhideOnExit;
        }

        public void SetUseUnhideOnExit(bool use)
        {
            useUnhideOnExit = use;
            WriteSettings();
        }

        public bool GetUseBorders()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return useBorders;
        }

        public void SetUseBorders(bool use)
        {
            useBorders = use;
            WriteSettings();
        }

        public bool GetUseToolTip()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return useTooltip;
        }

        public void SetUseTooltip(bool use)
        {
            useTooltip = use;
            WriteSettings();
        }

        public ThumbnailView GetThumbnailView()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return thumbnailView;
        }

        public void SetThumbnailView(ThumbnailView tView)
        {
            thumbnailView = tView;
            switch (tView)
            {
                case ThumbnailView.CachedAeroWindows:
                    break;
                case ThumbnailView.AeroWindows:
                    break;
                case ThumbnailView.BasicWindows:
                    break;
                default:
                    thumbnailView = ThumbnailView.CachedAeroWindows;
                    break;
            }
            WriteSettings();
        }

        public EmphasizeDesktop GetEmphasizeDesktop()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return emphasizeDesktop;
        }

        public void SetEmphasizeDesktop(EmphasizeDesktop eDesktop)
        {
            emphasizeDesktop = eDesktop;
            switch (eDesktop)
            {
                case EmphasizeDesktop.BorderedEmphasizer:
                    break;
                case EmphasizeDesktop.SmoothEmphasizer:
                    break;
                case EmphasizeDesktop.NoEmphasizer:
                    break;
                default:
                    emphasizeDesktop = EmphasizeDesktop.BorderedEmphasizer;
                    break;
            }
            WriteSettings();
        }

        public double GetUpdateThumbnailRate()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return updateThumbnailRate;
        }

        public void SetUpdateThumbnailRate(double utr)
        {
            updateThumbnailRate = Math.Min(Math.Max(utr, 0), 1);
            WriteSettings();
        }

        public List<HotKey> GetKeyLeft()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return keyLeft;
        }

        public void SetKeyLeft(List<HotKey> k)
        {
            keyLeft = k;
            WriteSettings();
        }

        public List<HotKey> GetKeyRight()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return keyRight;
        }

        public void SetKeyRight(List<HotKey> k)
        {
            keyRight = k;
            WriteSettings();
        }

        public List<HotKey> GetKeyMoveLeft()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return keyMoveLeft;
        }

        public void SetKeyMoveLeft(List<HotKey> k)
        {
            keyMoveLeft = k;
            WriteSettings();
        }

        public List<HotKey> GetKeyMoveRight()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return keyMoveRight;
        }

        public void SetKeyMoveRight(List<HotKey> k)
        {
            keyMoveRight = k;
            WriteSettings();
        }

        public List<HotKey> GetKeyShow()
        {
            if (!readSettings)
            {
                ReadSettings();
            }
            return keyShow;
        }

        public void SetKeyShow(List<HotKey> k)
        {
            keyShow = k;
            WriteSettings();
        }

        private HotKey Par2Hotkey(bool modifier_alt, bool modifier_ctrl, bool modifier_shift, bool modifier_windows, int key)
        {
            HotKey k = new HotKey();
            k.key = (Keys)key;
            k.keyModifier = (KeyModifier)0;
            if (modifier_alt)
            {
                k.keyModifier = k.keyModifier | KeyModifier.KM_ALT;
            }
            if(modifier_ctrl)
            {
                k.keyModifier = k.keyModifier | KeyModifier.KM_CTRL;
            }
            if(modifier_shift)
            {
                k.keyModifier = k.keyModifier | KeyModifier.KM_SHIFT;
            }
            if(modifier_windows)
            {
                k.keyModifier = k.keyModifier | KeyModifier.KM_WINDOWS;
            }
            return k;
        }

        public void ReadSettings()
        {
            String DesktopCountText = "";
            String StartingWithWindowsText = "";
            String PreviewHeightText = "";
            String UseAeroText = "";
            String UseAddCloseButtonsText = "";
            String UseUnhideOnExitText = "";
            String UseBordersText = "";
            String UseTooltipText = "";
            String ThumbnailViewText = "";
            String EmphasizeDesktopText = "";
            String UpdateThumbnailRateText = "";
            FileStream s = null;
            mutex.WaitOne();
            try
            {
                s = new FileStream(GetSettingsDirectory() + "\\VDesktop.xml", FileMode.Open, FileAccess.Read);
                XmlReader settingReader = new XmlTextReader(new StreamReader(s));
                String currNodeName = "";
                bool modifier_alt = false;
                bool modifier_ctrl = false;
                bool modifier_shit = false;
                bool modifier_windows = false;
                int key = 0;
                while (settingReader.Read())
                {
                    switch (settingReader.NodeType)
                    {
                        case XmlNodeType.Element:
                            {
                                currNodeName = settingReader.Name;
                                while (settingReader.MoveToNextAttribute())
                                {
                                    if (settingReader.Name.Equals("UpdateThumbnailRate"))
                                    {
                                        UpdateThumbnailRateText = settingReader.Value;
                                    }
                                    else if (settingReader.Name.Equals("alt"))
                                    {
                                        if (settingReader.Value.Equals("true"))
                                        {
                                            modifier_alt = true;
                                        }
                                    }
                                    else if (settingReader.Name.Equals("ctrl"))
                                    {
                                        if (settingReader.Value.Equals("true"))
                                        {
                                            modifier_ctrl = true;
                                        }
                                    }
                                    else if (settingReader.Name.Equals("shift"))
                                    {
                                        if (settingReader.Value.Equals("true"))
                                        {
                                            modifier_shit = true;
                                        }
                                    }
                                    else if (settingReader.Name.Equals("windows"))
                                    {
                                        if (settingReader.Value.Equals("true"))
                                        {
                                            modifier_windows = true;
                                        }
                                    }
                                    else if (settingReader.Name.Equals("key"))
                                    {
                                        try
                                        {
                                            key = Int32.Parse(settingReader.Value);
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                }

                                if (currNodeName.Equals("KeyLeft"))
                                {
                                    keyLeft.Add(Par2Hotkey(modifier_alt, modifier_ctrl, modifier_shit, modifier_windows, key));
                                }
                                else if (currNodeName.Equals("KeyRight"))
                                {
                                    keyRight.Add(Par2Hotkey(modifier_alt, modifier_ctrl, modifier_shit, modifier_windows, key));
                                }
                                else if (currNodeName.Equals("KeyMoveLeft"))
                                {
                                    keyMoveLeft.Add(Par2Hotkey(modifier_alt, modifier_ctrl, modifier_shit, modifier_windows, key));
                                }
                                else if (currNodeName.Equals("KeyMoveRight"))
                                {
                                    keyMoveRight.Add(Par2Hotkey(modifier_alt, modifier_ctrl, modifier_shit, modifier_windows, key));
                                }
                                else if (currNodeName.Equals("KeyShow"))
                                {
                                    keyShow.Add(Par2Hotkey(modifier_alt, modifier_ctrl, modifier_shit, modifier_windows, key));
                                }

                                modifier_alt = false;
                                modifier_ctrl = false;
                                modifier_shit = false;
                                modifier_windows = false;
                                key = 0;
                            }
                            break;
                        case XmlNodeType.Text:
                            {
                                if (currNodeName.Equals("DesktopCount"))
                                {
                                    DesktopCountText = settingReader.Value;
                                }
                                else if (currNodeName.Equals("StartingWithWindows"))
                                {
                                    StartingWithWindowsText = settingReader.Value;
                                }
                                else if (currNodeName.Equals("PreviewHeight"))
                                {
                                    PreviewHeightText = settingReader.Value;
                                }
                                else if (currNodeName.Equals("UseAero"))
                                {
                                    UseAeroText = settingReader.Value;
                                }
                                else if (currNodeName.Equals("UseAddCloseButtons"))
                                {
                                    UseAddCloseButtonsText = settingReader.Value;
                                }
                                else if (currNodeName.Equals("UseUnhideOnExit"))
                                {
                                    UseUnhideOnExitText = settingReader.Value;
                                }
                                else if (currNodeName.Equals("UseBorders"))
                                {
                                    UseBordersText = settingReader.Value;
                                }
                                else if (currNodeName.Equals("UseTooltip"))
                                {
                                    UseTooltipText = settingReader.Value;
                                }
                                else if (currNodeName.Equals("ThumbnailView"))
                                {
                                    ThumbnailViewText = settingReader.Value;
                                }
                                else if (currNodeName.Equals("EmphasizeDesktop"))
                                {
                                    EmphasizeDesktopText = settingReader.Value;
                                }
                                else if (currNodeName.Equals("UpdateThumbnailRate"))
                                {
                                    UpdateThumbnailRateText = settingReader.Value;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
                readSettings = true;
            }
            catch (Exception)
            {
                HotKey show1 = new HotKey();
                show1.keyModifier = KeyModifier.KM_WINDOWS | KeyModifier.KM_ALT;
                show1.key = Keys.LWin;
                HotKey show2 = new HotKey();
                show2.keyModifier = KeyModifier.KM_CTRL | KeyModifier.KM_WINDOWS;
                show2.key = Keys.ControlKey;
                HotKey left1 = new HotKey();
                left1.keyModifier = KeyModifier.KM_CTRL | KeyModifier.KM_WINDOWS;
                left1.key = Keys.Left;
                HotKey left2 = new HotKey();
                left2.keyModifier = KeyModifier.KM_CTRL | KeyModifier.KM_WINDOWS;
                left2.key = Keys.A;
                HotKey right1 = new HotKey();
                right1.keyModifier = KeyModifier.KM_CTRL | KeyModifier.KM_WINDOWS;
                right1.key = Keys.Right;
                HotKey right2 = new HotKey();
                right2.keyModifier = KeyModifier.KM_CTRL | KeyModifier.KM_WINDOWS;
                right2.key = Keys.D;
                HotKey moveLeft1 = new HotKey();
                moveLeft1.keyModifier = KeyModifier.KM_WINDOWS | KeyModifier.KM_ALT;
                moveLeft1.key = Keys.Left;
                HotKey moveLeft2 = new HotKey();
                moveLeft2.keyModifier = KeyModifier.KM_WINDOWS | KeyModifier.KM_ALT;
                moveLeft2.key = Keys.A;
                HotKey moveRight1 = new HotKey();
                moveRight1.keyModifier = KeyModifier.KM_WINDOWS | KeyModifier.KM_ALT;
                moveRight1.key = Keys.Right;
                HotKey moveRight2 = new HotKey();
                moveRight2.keyModifier = KeyModifier.KM_WINDOWS | KeyModifier.KM_ALT;
                moveRight2.key = Keys.D;

                keyShow.Add(show1);
                keyShow.Add(show2);
                keyLeft.Add(left1);
                keyLeft.Add(left2);
                keyRight.Add(right1);
                keyRight.Add(right2);
                keyMoveLeft.Add(moveLeft1);
                keyMoveLeft.Add(moveLeft2);
                keyMoveRight.Add(moveRight1);
                keyMoveRight.Add(moveRight2);
            }
            try
            {
                s.Close();
                s.Dispose();
            }
            catch (Exception)
            {
            }
            mutex.ReleaseMutex();

            try
            {
                int n = Int32.Parse(DesktopCountText);
                desktopCount = Math.Max(1, n);
            }
            catch (Exception)
            {
                desktopCount = 4;
            }

            try
            {
                startingWithWindows = Boolean.Parse(StartingWithWindowsText);
            }
            catch (Exception)
            {
                startingWithWindows = false;
            }

            try
            {
                int ps = Int32.Parse(PreviewHeightText);
                previewHeight = Math.Max(10, ps);
            }
            catch (Exception)
            {
                previewHeight = 150;
            }

            try
            {
                useAero = Boolean.Parse(UseAeroText);
            }
            catch (Exception)
            {
                useAero = true;
            }

            try
            {
                useAddCloseButtons = Boolean.Parse(UseAddCloseButtonsText);
            }
            catch (Exception)
            {
                useAddCloseButtons = true;
            }

            try
            {
                useUnhideOnExit = Boolean.Parse(UseUnhideOnExitText);
            }
            catch (Exception)
            {
                useUnhideOnExit = true;
            }

            try
            {
                useBorders = Boolean.Parse(UseBordersText);
            }
            catch (Exception)
            {
                useBorders = true;
            }

            try
            {
                useTooltip = Boolean.Parse(UseTooltipText);
            }
            catch (Exception)
            {
                useTooltip = true;
            }

            if(ThumbnailViewText.Equals(CachedAeroWindows))
            {
                thumbnailView = ThumbnailView.CachedAeroWindows;
            }
            else if(ThumbnailViewText.Equals(AeroWindows))
            {
                thumbnailView = ThumbnailView.AeroWindows;
            }
            else if (ThumbnailViewText.Equals(BasicWindows))
            {
                thumbnailView = ThumbnailView.BasicWindows;
            }
            else
            {
                thumbnailView = ThumbnailView.CachedAeroWindows;
            }

            if (EmphasizeDesktopText.Equals(BorderedEmphasizer))
            {
                emphasizeDesktop = EmphasizeDesktop.BorderedEmphasizer;
            }
            else if (EmphasizeDesktopText.Equals(SmoothEmphasizer))
            {
                emphasizeDesktop = EmphasizeDesktop.SmoothEmphasizer;
            }
            else if (EmphasizeDesktopText.Equals(NoEmphasizer))
            {
                emphasizeDesktop = EmphasizeDesktop.NoEmphasizer;
            }
            else
            {
                emphasizeDesktop = EmphasizeDesktop.BorderedEmphasizer;
            }

            try
            {
                updateThumbnailRate = Double.Parse(UpdateThumbnailRateText);
            }
            catch (Exception)
            {
                updateThumbnailRate = 0.3;
            }
        }

        private XElement Hotkey2XElement(String name, HotKey hotKey)
        {
            String ctrl = "false";
            String shift = "false";
            String alt = "false";
            String windows = "false";
            if ((hotKey.keyModifier & KeyModifier.KM_CTRL) != 0)
            {
                ctrl = "true";
            }
            if((hotKey.keyModifier & KeyModifier.KM_SHIFT) != 0)
            {
                shift = "true";
            }
            if((hotKey.keyModifier & KeyModifier.KM_ALT) != 0)
            {
                alt = "true";
            }
            if ((hotKey.keyModifier & KeyModifier.KM_WINDOWS) != 0)
            {
                windows = "true";
            }
            XElement e = new XElement(name,
                new XAttribute("alt", alt),
                new XAttribute("ctrl", ctrl),
                new XAttribute("shift", shift),
                new XAttribute("windows", windows),
                new XAttribute("key", "" + ((int)hotKey.key))
                );

            return e;
        }

        public void WriteSettings()
        {
            XElement hotkeysElement = new XElement("Hotkeys");
            foreach (HotKey hotKey in keyLeft)
            {
                XElement e = Hotkey2XElement("KeyLeft", hotKey);
                hotkeysElement.Add(e);
            }
            foreach (HotKey hotKey in keyRight)
            {
                XElement e = Hotkey2XElement("KeyRight", hotKey);
                hotkeysElement.Add(e);
            }
            foreach (HotKey hotKey in keyMoveLeft)
            {
                XElement e = Hotkey2XElement("KeyMoveLeft", hotKey);
                hotkeysElement.Add(e);
            }
            foreach (HotKey hotKey in keyMoveRight)
            {
                XElement e = Hotkey2XElement("KeyMoveRight", hotKey);
                hotkeysElement.Add(e);
            }
            foreach (HotKey hotKey in keyShow)
            {
                XElement e = Hotkey2XElement("KeyShow", hotKey);
                hotkeysElement.Add(e);
            }

            XElement startingWithWindowsElement =
                new XElement("StartingWithWindows",
                    new XText("" + startingWithWindows)
                    );

            XElement previewHeightElement =
                new XElement("PreviewHeight",
                    new XText("" + previewHeight)
                    );

            XElement useAeroElement =
                new XElement("UseAero",
                    new XText("" + useAero)
                    );

            XElement useAddCloseButtonsElement =
                new XElement("UseAddCloseButtons",
                    new XText("" + useAddCloseButtons)
                    );

            XElement useUnhideOnExitElement =
                new XElement("UseUnhideOnExit",
                    new XText("" + useUnhideOnExit)
                    );

            XElement useBordersElement =
                new XElement("UseBorders",
                    new XText("" + useBorders)
                    );

            XElement useTooltipElement =
                new XElement("UseTooltip",
                    new XText("" + useTooltip)
                    );

            String thumbnailViewText;
            switch(thumbnailView)
            {
                case ThumbnailView.CachedAeroWindows:
                    thumbnailViewText = Settings.CachedAeroWindows;
                    break;
                case ThumbnailView.AeroWindows:
                    thumbnailViewText = Settings.AeroWindows;
                    break;
                case ThumbnailView.BasicWindows:
                    thumbnailViewText = Settings.BasicWindows;
                    break;
                default:
                    thumbnailViewText = Settings.CachedAeroWindows;
                    break;
            }
            XElement thumbnailViewElement = null;
            if(thumbnailView == ThumbnailView.CachedAeroWindows)
            {
                thumbnailViewElement = 
                    new XElement("ThumbnailView",
                        new XText(thumbnailViewText),
                        new XAttribute("UpdateThumbnailRate", "" + updateThumbnailRate)
                        );
            }
            else
            {
                thumbnailViewElement = 
                    new XElement("ThumbnailView",
                        new XText(thumbnailViewText)
                        );
            }

            String emphasizeDesktopText;
            switch (emphasizeDesktop)
            {
                case EmphasizeDesktop.BorderedEmphasizer:
                    emphasizeDesktopText = Settings.BorderedEmphasizer;
                    break;
                case EmphasizeDesktop.SmoothEmphasizer:
                    emphasizeDesktopText = Settings.SmoothEmphasizer;
                    break;
                case EmphasizeDesktop.NoEmphasizer:
                    emphasizeDesktopText = Settings.NoEmphasizer;
                    break;
                default:
                    emphasizeDesktopText = Settings.BorderedEmphasizer;
                    break;
            }
            XElement emphasizeDesktopElement =
                new XElement("EmphasizeDesktop",
                    new XText(emphasizeDesktopText)
                    );

            XElement xmlSettings =
                new XElement("VDesktop",
                    new XElement("DesktopCount",
                        new XText("" + desktopCount)
                        ),
                    startingWithWindowsElement,
                    previewHeightElement,
                    useAeroElement,
                    useAddCloseButtonsElement,
                    useUnhideOnExitElement,
                    useBordersElement,
                    useTooltipElement,
                    thumbnailViewElement,
                    emphasizeDesktopElement,
                    hotkeysElement
                    );
            FileStream s = null;
            mutex.WaitOne();
            try
            {
                s = new FileStream(GetSettingsDirectory() + "\\VDesktop.xml", FileMode.Create, FileAccess.Write, FileShare.Write);
                xmlSettings.Save(new StreamWriter(s));
            }
            catch (Exception)
            {
            }
            try
            {
                s.Close();
                s.Dispose();
            }
            catch (Exception)
            {
            }
            mutex.ReleaseMutex();
        }

    }

}
