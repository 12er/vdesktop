﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using VDesktop.NativeCalls;

namespace VDesktop
{
    class WindowRecovery
    {
        private static WindowRecovery windowRecovery = null;

        public static WindowRecovery Get()
        {
            if(windowRecovery == null)
            {
                windowRecovery = new WindowRecovery();
            }
            return windowRecovery;
        }

        private UDictionary<IntPtr, uint> savedWindows = new UDictionary<IntPtr,uint>();
        private UDictionary<IntPtr, uint> newSavedWindows = new UDictionary<IntPtr, uint>();
        private System.Timers.Timer timer = new System.Timers.Timer();
        private Mutex mutex = new Mutex();

        public WindowRecovery()
        {
        }

        public void StartSavePeriodically()
        {
            timer.Elapsed += new System.Timers.ElapsedEventHandler(TimerSaveEvent);
            timer.Interval = 5000;
            timer.Start();
        }

        public void StopSavePeriodically()
        {
            timer.Stop();
        }

        public void TimerSaveEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            timer.Stop();
            try
            {
                SaveWindows();
            }
            catch (Exception)
            {
            }
            timer.Start();
        }

        public void RestoreWindows()
        {
            //read stored windows
            mutex.WaitOne();
            FileStream s = null;
            try
            {
                s = new FileStream(Settings.GetSettingsDirectory() + "\\WindowRecovery.xml", FileMode.Open, FileAccess.Read);
                XmlReader recovery = new XmlTextReader(new StreamReader(s));
                while (recovery.Read())
                {
                    switch (recovery.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (recovery.Name.Equals("Window"))
                            {
                                String currNodeName = recovery.Name;
                                IntPtr hWnd = IntPtr.Zero;
                                uint deskID = 0;
                                bool parsedHWnd = false;
                                bool parsedDeskID = false;
                                while (recovery.MoveToNextAttribute())
                                {
                                    if (recovery.Name.Equals("hWnd"))
                                    {
                                        try
                                        {
                                            String val = recovery.Value;
                                            long lval = long.Parse(val);
                                            hWnd = new IntPtr(lval);
                                            parsedHWnd = true;
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                    else if (recovery.Name.Equals("VDesktop"))
                                    {
                                        try
                                        {
                                            String val = recovery.Value;
                                            deskID = UInt32.Parse(val);
                                            parsedDeskID = true;
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                }
                                if (parsedHWnd && parsedDeskID)
                                {
                                    savedWindows[hWnd] = deskID;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            try
            {
                s.Close();
                s.Dispose();
            }
            catch (Exception)
            {
            }
            mutex.ReleaseMutex();

            //insert windows in vdesktops
            DesktopsForm overview = DesktopsForm.Get();
            overview.Invoke(new Action(() =>
                {
                    mutex.WaitOne();
                    VDesktopManager vdm = VDesktopManager.Get();
                    int deskCount = vdm.GetDesktops().Count;
                    foreach (IntPtr hWnd in savedWindows.Keys)
                    {
                        try
                        {
                            foreach (VDesktop vd in vdm.GetDesktops())
                            {
                                if (vd.GetWindows().ContainsKey(hWnd))
                                {
                                    vd.GetWindows().Remove(hWnd);
                                }
                            }
                            int deskID = Math.Max(1, Math.Min(deskCount, (int)savedWindows[hWnd]));
                            VDesktop desk = vdm.GetDesktops()[deskID-1];
                            VDesktopWindow window = new VDesktopWindow(hWnd);
                            if (window.IsValid())
                            {
                                desk.GetWindows()[hWnd] = window;
                                if (desk == vdm.GetCurrentDesktop())
                                {
                                    window.Show();
                                }
                                else
                                {
                                    window.Hide();
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                    mutex.ReleaseMutex();
                }
                )
            );
            mutex.WaitOne();
            mutex.ReleaseMutex();

            //install timer saving windows state regularly
        }

        public void SaveWindows()
        {
            //collect windows
            newSavedWindows.Clear();
            User32.EnumWindows(EnumWindowsProc, IntPtr.Zero);
            DesktopsForm overview = DesktopsForm.Get();
            overview.Invoke(new Action(() =>
                {
                    mutex.WaitOne();
                    VDesktopManager vdm = VDesktopManager.Get();

                    try
                    {
                        foreach (VDesktop vdesk in vdm.GetDesktops())
                        {
                            uint deskID = vdesk.getID();
                            try
                            {
                                foreach (IntPtr hWnd in vdesk.GetWindows().Keys)
                                {
                                    newSavedWindows[hWnd] = deskID;
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                    mutex.ReleaseMutex();
                }
            ));
            mutex.WaitOne();

            //has anything changed? if that's not the case, there is nothing to do
            if (savedWindows.Count == newSavedWindows.Count)
            {
                bool nothingToDo = true;
                foreach (IntPtr hWnd in newSavedWindows.Keys)
                {
                    if (!savedWindows.ContainsKey(hWnd))
                    {
                        nothingToDo = false;
                        break;
                    }
                    if (savedWindows[hWnd] != newSavedWindows[hWnd])
                    {
                        nothingToDo = false;
                        break;
                    }
                }
                if (nothingToDo)
                {
                    mutex.ReleaseMutex();
                    return;
                }
            }

            //save collected windows
            savedWindows.Clear();
            foreach (IntPtr hWnd in newSavedWindows.Keys)
            {
                savedWindows[hWnd] = newSavedWindows[hWnd];
            }
            XElement xmlWindowRecovery = new XElement("WindowRecovery");
            foreach (IntPtr hWnd in savedWindows.Keys)
            {
                XElement window = new XElement("Window",
                    new XAttribute("hWnd", "" + hWnd.ToInt64()),
                    new XAttribute("VDesktop", "" + savedWindows[hWnd])
                    );
                xmlWindowRecovery.Add(window);
            }
            FileStream s = null;
            try
            {
                s = new FileStream(Settings.GetSettingsDirectory() + "\\WindowRecovery.xml", FileMode.Create, FileAccess.Write, FileShare.Write);
                xmlWindowRecovery.Save(new StreamWriter(s));
            }
            catch (Exception)
            {
            }
            try
            {
                s.Close();
                s.Dispose();
            }
            catch (Exception)
            {
            }
            mutex.ReleaseMutex();
        }

        public void ForcedSaveWindows()
        {
            savedWindows.Clear();
            newSavedWindows.Clear();
            SaveWindows();
        }

        private bool EnumWindowsProc(IntPtr hWnd, IntPtr lParams)
        {
            VDesktopManager vdm = VDesktopManager.Get();
            UDictionary<IntPtr, VDesktopWindow> ignoredWindows = vdm.GetIgnoredWindows();
            uint deskID = vdm.GetCurrentDesktop().getID();

            //valid visible window? collect it
            VDesktopWindow window = new VDesktopWindow(hWnd);
            if (window.IsDesktopWindow() && window.IsVisible() && !ignoredWindows.ContainsKey(hWnd))
            {
                newSavedWindows[hWnd] = deskID;
            }

            return true;
        }

    }
}
