To start the app on startup, first edit Task.xml, replace UserId by Windows username, and Command and Workingdirectory by the absolute paths to VDesktop.exe. Then call

schtasks /Create /tn MyProgram /XML path-to-Task.xml

such that path-to-Task.xml points to Task.xml.

Undo by calling

schtasks /Delete /tn MyProgram